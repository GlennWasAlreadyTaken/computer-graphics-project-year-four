#include <ShaderProgram.hpp>
#include <Viewer.hpp>

#include <Utils.hpp>
#include <KeyframedCylinderRenderable.hpp>
#include <FrameRenderable.hpp>
#include <GeometricTransformation.hpp>
#include <iostream>
#include <iomanip>

#include <HierarchicalMeshRenderable.hpp>
#include <texturing/TexturedLightedMeshRenderable.hpp>
#include <KeyframedMeshRenderable.hpp>
#include <lighting/DirectionalLightRenderable.hpp>
#include <lighting/PointLightRenderable.hpp>
#include <lighting/Light.hpp>
#include <Movie.hpp>
/*
//MiniScenes
void OutOfLightSpeed(Viewer& viewer, ShaderProgramPtr& shader, float& timeStamp);
void cowAbducting(Viewer& viewer, ShaderProgramPtr& shader, float& timeStamp);
void cowStage(Viewer& viewer, ShaderProgramPtr& shader, float& timeStamp);
void ufoOutOfMotherShip(Viewer& viewer, ShaderProgramPtr& shader, float& timeStamp);
void ufoToEarth(Viewer &viewer, ShaderProgramPtr &shader, ShaderProgramPtr &textureReflectSkyboxShader, float& timeStamp);
void ufoToSpace(Viewer &viewer, ShaderProgramPtr &shader, ShaderProgramPtr &textureReflectSkyboxShader, float& timeStamp);
void runningAliens(Viewer& viewer, ShaderProgramPtr& shader, float& timeStamp);
void docking(Viewer& viewer, ShaderProgramPtr& shader, float& timeStamp);

#include <HierarchicalMeshRenderable.hpp>
#include <texturing/TexturedLightedMeshRenderable.hpp>
#include <KeyframedMeshRenderable.hpp>
#include <lighting/DirectionalLightRenderable.hpp>
#include <lighting/PointLightRenderable.hpp>
#include <lighting/Light.hpp>


void loadSpaceSkybox(Viewer &viewer);
void loadEarthSkybox(Viewer &viewer);

//Animations
void OutOfLightSpeed(Viewer& viewer, KeyframedMeshRenderablePtr& spaceShip);
void ufoOutOfMotherShip(Viewer &viewer, KeyframedMeshRenderablePtr &motherShip, KeyframedMeshRenderablePtr &ufo);
void ufoToEarth(Viewer& viewer, KeyframedMeshRenderablePtr& ufo);
void runningAliens(Viewer& viewer, KeyframedMeshRenderablePtr legs[], float delay);
void makeStep(Viewer& viewer, KeyframedMeshRenderablePtr leg, float delay, glm::vec3 offset);
void docking(Viewer& viewer, KeyframedMeshRenderablePtr& dock, KeyframedMeshRenderablePtr& ufo);
*/
int main()
{
	Viewer viewer(1280, 720);
	//initialize_scene(viewer);
	Movie mov(viewer);
	mov.start();

	GLenum err;
	while ((err = glGetError()) != GL_NO_ERROR)
	{
		// Process/log the error.
		std::cout << err << std::endl;
		return 0;
	}

	while (viewer.isRunning())
	{
		viewer.handleEvent();
		viewer.animate();
		viewer.draw();
		viewer.display();
	}

	return EXIT_SUCCESS;
}
/*
void initialize_scene( Viewer& viewer )
{
	Camera &cam = viewer.getCamera();
	cam.setMouseBehavior(Camera::CAMERA_MOUSE_BEHAVIOR::SPACESHIP_BEHAVIOR);

	//Add shader
	ShaderProgramPtr flatShader = std::make_shared<ShaderProgram>("../../sfmlGraphicsPipeline/shaders/flatVertex.glsl",
		"../../sfmlGraphicsPipeline/shaders/flatFragment.glsl");
	viewer.addShaderProgram(flatShader);

    ShaderProgramPtr textureShader = std::make_shared<ShaderProgram>("../../sfmlGraphicsPipeline/shaders/textureVertex.glsl",
                                                                     "../../sfmlGraphicsPipeline/shaders/textureFragment.glsl");
	viewer.addShaderProgram(textureShader);

	ShaderProgramPtr textureReflectSkyboxShader = std::make_shared<ShaderProgram>("../../sfmlGraphicsPipeline/shaders/textureVertex.glsl",
																	 			  "../../sfmlGraphicsPipeline/shaders/textureFragmentReflectSkybox.glsl");
	viewer.addShaderProgram(textureReflectSkyboxShader);

	//Frame
	FrameRenderablePtr frame = std::make_shared<FrameRenderable>(flatShader);
	viewer.addRenderable(frame);

	float timeStamp = 0;

	// In the film
	//OutOfLightSpeed(viewer, textureReflectSkyboxShader, timeStamp);
	
	//runningAliens(viewer, textureShader, timeStamp);
	
	docking(viewer, textureShader, timeStamp);
	
	//ufoOutOfMotherShip(viewer, textureReflectSkyboxShader, timeStamp);
	
	//ufoToEarth(viewer, textureShader, textureReflectSkyboxShader, timeStamp);
	
	//ufoToSpace(viewer, textureShader, textureReflectSkyboxShader, timeStamp);
	
	//cowAbducting(viewer, textureShader, timeStamp);
	
	//cowStage(viewer, textureShader, timeStamp);s
}

void loadSpaceSkybox(Viewer& viewer)
{
	ShaderProgramPtr shader = std::make_shared<ShaderProgram>("../../sfmlGraphicsPipeline/shaders/skyboxVertex.glsl",
																  "../../sfmlGraphicsPipeline/shaders/skyboxFragment.glsl");
	viewer.addShaderProgram(shader);

	std::string path = "../../sfmlGraphicsPipeline/textures/skybox/space_skybox/";
	std::vector<std::string> faces =
		{
			path + "right.png",
			path + "left.png",
			path + "top.png",
			path + "bottom.png",
			path + "front.png",
			path + "back.png"
			};

	viewer.setSkybox(std::make_shared<SkyboxRenderable>(shader, faces));

	// Defining the light of the space skybox
	glm::vec3 lightPosition(-35.0, 17.0, 0.0);
	glm::vec3 d_direction = glm::normalize(-lightPosition);
	glm::vec3 d_ambient(0.1, 0.1, 0.1), d_diffuse(0.8, 0.8, 0.8), d_specular(0.8, 0.8, 0.8);
	DirectionalLightPtr directionalLight = std::make_shared<DirectionalLight>(d_direction, d_ambient, d_diffuse, d_specular);

	viewer.setDirectionalLight(directionalLight);
}

void loadEarthSkybox(Viewer & viewer)
{
	ShaderProgramPtr shader = std::make_shared<ShaderProgram>("../../sfmlGraphicsPipeline/shaders/skyboxVertex.glsl",
															  "../../sfmlGraphicsPipeline/shaders/skyboxFragment.glsl");
	viewer.addShaderProgram(shader);

	std::string path = "../../sfmlGraphicsPipeline/textures/skybox/hill/";
	std::vector<std::string> faces =
		{
			path + "px.jpg",
			path + "nx.jpg",
			path + "py.jpg",
			path + "ny.jpg",
			path + "pz.jpg",
			path + "nz.jpg"};

	viewer.setSkybox(std::make_shared<SkyboxRenderable>(shader, faces));

	// Defining the light of the space skybox
	glm::vec3 lightPosition(1.6, 233.7, 175.6);
	glm::vec3 d_direction = glm::normalize(glm::vec3(0, 30, 0) - lightPosition);
	glm::vec3 d_ambient(0.1, 0.1, 0.1), d_diffuse(0.8, 0.8, 0.8), d_specular(0.8, 0.8, 0.8);
	DirectionalLightPtr directionalLight = std::make_shared<DirectionalLight>(d_direction, d_ambient, d_diffuse, d_specular);

	viewer.setDirectionalLight(directionalLight);
}

//MiniScenes
void OutOfLightSpeed(Viewer &viewer, ShaderProgramPtr &shader, float& timeStamp)
{
	loadSpaceSkybox(viewer);

	glm::vec3 camPos{-3.26, 14.76, -15.87};
	glm::vec3 camForward{0.667, -0.449, 0.593};
	glm::vec3 camUp{0.249, 0.886, 0.389};

	viewer.getCamera().setViewMatrix(glm::lookAt(
		camPos,
		camPos + camForward,
		camUp));

	KeyframedMeshRenderablePtr spaceShip = std::make_shared<KeyframedMeshRenderable>(shader, "../../sfmlGraphicsPipeline/meshes/Mothership/vaisseau_mere.obj");
	spaceShip->setLocalTransform(GeometricTransformation(glm::vec3{ 0,0,0 }, glm::quat(glm::vec3{ 0,0,0 }), glm::vec3{ 5,5,5 }).toMatrix());
	viewer.addRenderable(spaceShip);
	//========= Animation =========//

	OutOfLightSpeed(viewer, spaceShip, timeStamp);
	viewer.startAnimation();
}

void cowAbducting(Viewer& viewer, ShaderProgramPtr& shader, float& timeStamp) {
	
	loadEarthSkybox(viewer);

	glm::vec3 camPos{ -16.93, 15.08, 14.64 };
	glm::vec3 camForward{ -0.086, 0.40, -0.91 };
	glm::vec3 camUp{ -0.04, 0.91, 0.41 };

	viewer.getCamera().setViewMatrix(glm::lookAt(
		camPos,
		camPos + camForward,
		camUp));

	// UFO
	KeyframedMeshRenderablePtr ufo = std::make_shared<KeyframedMeshRenderable>(shader, "../../sfmlGraphicsPipeline/meshes/UFO/UFO_COLORED.obj");
	glm::vec3 ufoStartPos = { 15, 60, 10 };
	ufo->setLocalTransform(GeometricTransformation(ufoStartPos, glm::quat(glm::vec3{0, 0, 0}), glm::vec3{3, 3, 3}).toMatrix());
    viewer.addRenderable(ufo);

	TexturedLightedMeshRenderablePtr hill = std::make_shared<TexturedLightedMeshRenderable>(shader, "../../sfmlGraphicsPipeline/meshes/Hill/Hill.obj");
	hill->setLocalTransform(GeometricTransformation(glm::vec3{-10, 20, -10}, glm::quat(glm::vec3{0, 0, 0}), glm::vec3{3, 3, 3}).toMatrix());
	viewer.addRenderable(hill);
  
    //Add a renderable to display the light and control it via mouse/key event
*//*
    DirectionalLightRenderablePtr directionalLightRenderable = std::make_shared<DirectionalLightRenderable>(shader, directionalLight, lightPosition);
    localTransformation = glm::scale(glm::mat4(1.0), glm::vec3(0.5, 0.5, 0.5));
    directionalLightRenderable->setLocalTransform(localTransformation);
    viewer.addRenderable(directionalLightRenderable);
*//*
  
	glm::vec3 cowStartPos = { -26,13,-4 };

    KeyframedMeshRenderablePtr cow = std::make_shared<KeyframedMeshRenderable>(shader, "../../sfmlGraphicsPipeline/meshes/cow/cow.obj");
    cow->setLocalTransform(GeometricTransformation(cowStartPos, glm::quat(glm::vec3{ 0,0,0 }), glm::vec3{ 0.1,0.1,0.1 }).toMatrix());

	float animScale = 10.0f;
    float abductTime = 4.0f;
	int nbOfKeyframes = 8;
	float currentTime = abductTime /nbOfKeyframes;
	float timeBeforeAbduct = 2.0f;

	//Animate cow
	GeometricTransformation gt1 = getGT(cowStartPos + animScale * glm::vec3{ 0,0,0 }, glm::vec3{ 0,0,0 }, glm::vec3{ 0.1,0.1,0.1 });
	cow->addLocalTransformKeyframe(gt1,0.0f);
	cow->addLocalTransformKeyframe(gt1, timeBeforeAbduct + 0.0f);
	
	for (int i = 0; i < nbOfKeyframes; i++) {
		GeometricTransformation gt = getGT(cowStartPos + animScale * glm::vec3{ random(-0.05,0.05)* (1-currentTime/abductTime), animScale * std::max((exp(currentTime)/exp(abductTime/2)),1.0f) / (10 * abductTime),random(-0.05,0.05) * (1 - currentTime / abductTime) }, glm::vec3{ random(-M_PI / 16, M_PI / 16) * (1 - currentTime / abductTime),random(-M_PI / 16,M_PI / 16) * (1 - currentTime / abductTime),random(-M_PI / 16,M_PI / 16) * (1 - currentTime / abductTime) }, glm::vec3{ 0.1,0.1,0.1 });

		cow->addLocalTransformKeyframe(gt, timeBeforeAbduct + currentTime);
		currentTime += abductTime / nbOfKeyframes;
	}
	GeometricTransformation gtbegin = getGT(ufoStartPos, glm::vec3{ 0,0,0 }, glm::vec3{ 0.1f,0.1f,0.1f });
	GeometricTransformation gtend = getGT(glm::vec3{ -26,32,-4 }, glm::vec3{ 0,0,0 }, glm::vec3{ 0.1f,0.1f,0.1f });
	//Animate cow when in ufo
	cow->addLocalTransformKeyframe(gtend, timeBeforeAbduct + abductTime);
	cow->addLocalTransformKeyframe(gtbegin, timeBeforeAbduct + abductTime + 2.0f);

	//Animate ufo
	gtbegin = getGT(ufoStartPos, glm::vec3{ 0,0,0 }, glm::vec3{ 3,3,3 });
	gtend = getGT(glm::vec3{ -26,34,-4 }, glm::vec3{ 0,M_PI/2,0 }, glm::vec3{ 3,3,3 });

	ufo->addLocalTransformKeyframe(gtbegin, 0.0f);
	ufo->addLocalTransformKeyframe(gtend, timeBeforeAbduct);
	gtend = getGT(glm::vec3{ -26,34,-4 }, glm::vec3{ 0,3*M_PI/4,0 }, glm::vec3{ 3,3,3 });
	ufo->addLocalTransformKeyframe(gtend, timeBeforeAbduct + abductTime);
	gtbegin = getGT(ufoStartPos, glm::vec3{ 0,M_PI,0 }, glm::vec3{ 3,3,3 });
	ufo->addLocalTransformKeyframe(gtbegin, timeBeforeAbduct + abductTime + 2.0f);
	
	
	viewer.addRenderable(cow);

	viewer.startAnimation();
	viewer.setAnimationLoop(true, timeBeforeAbduct + abductTime + 2.0f);
}

void ufoOutOfMotherShip(Viewer& viewer, ShaderProgramPtr& shader, float& timeStamp) {

	loadSpaceSkybox(viewer);
	//Define a directional light for the whole scene
	glm::mat4 localTransformation;
	glm::vec3 lightPosition(-35.0, 17.0, 0.0);
	glm::vec3 d_direction = glm::normalize(-lightPosition);
	glm::vec3 d_ambient(0.3, 0.3, 0.3), d_diffuse(0.8, 0.8, 0.8), d_specular(0.8, 0.8, 0.8);
	DirectionalLightPtr directionalLight = std::make_shared<DirectionalLight>(d_direction, d_ambient, d_diffuse, d_specular);
	viewer.setDirectionalLight(directionalLight);

	glm::vec3 camPos{ 1.7, 2.0, -3.6 };
	glm::vec3 camForward{ 0.83, -0.24, 0.49 };
	glm::vec3 camUp{ 0.20, 0.97, 0.12 };

	viewer.getCamera().setViewMatrix(glm::lookAt(
		camPos,
		camPos + camForward,
		camUp));

	//Object instanciations, adding them to viewer and creating a hierarchy
	KeyframedMeshRenderablePtr vaisseau_mere = std::make_shared<KeyframedMeshRenderable>(shader, "../../sfmlGraphicsPipeline/meshes/Mothership/vaisseau_mere.obj");
	vaisseau_mere->setLocalTransform(GeometricTransformation(glm::vec3{ 0,0,0 }, glm::quat(glm::vec3{ 0,0,0 }), glm::vec3{ 1,1,1 }).toMatrix());

	KeyframedMeshRenderablePtr ufo = std::make_shared<KeyframedMeshRenderable>(shader, "../../sfmlGraphicsPipeline/meshes/UFO/UFO_COLORED.obj");
	ufo->setLocalTransform(GeometricTransformation(glm::vec3{ 0,-0.2,0 }, glm::quat(glm::vec3{ 0,0,0 }), glm::vec3{ 0.01,0.01,0.01 }).toMatrix());

	viewer.addRenderable(vaisseau_mere);
	viewer.addRenderable(ufo);
	KeyframedMeshRenderable::addChild(vaisseau_mere, ufo);

	//========= Animation =========//

	ufoOutOfMotherShip(viewer, vaisseau_mere, ufo, timeStamp);
	viewer.startAnimation();
}

void ufoToEarth(Viewer &viewer, ShaderProgramPtr &shader, ShaderProgramPtr &textureReflectSkyboxShader, float& timeStamp)
{
	loadSpaceSkybox(viewer);

	glm::vec3 camPos{212.17, 64.41, -5.02};
	glm::vec3 camForward{-0.73, 0.04, 0.68};
	glm::vec3 camUp{-0.02, 0.99, -0.09};

	viewer.getCamera().setViewMatrix(glm::lookAt(
												camPos,
												camPos + camForward,
												camUp));

	//viewer.getCamera().setPosition(camPos);
	//viewer.getCamera().setForward(camForward);
	//viewer.getCamera().setRight(glm::normalize(glm::cross(camPos, camForward)));

	KeyframedMeshRenderablePtr ufo = std::make_shared<KeyframedMeshRenderable>(textureReflectSkyboxShader, "../../sfmlGraphicsPipeline/meshes/UFO/UFO_COLORED.obj");
	ufo->setLocalTransform(GeometricTransformation(glm::vec3{164, 73, -48}, glm::quat(glm::vec3{3.14 / 4, 3.14/2, 0}), glm::vec3{10, 10, 10}).toMatrix());
	viewer.addRenderable(ufo);

	KeyframedMeshRenderablePtr earth = std::make_shared<KeyframedMeshRenderable>(shader, "../../sfmlGraphicsPipeline/meshes/earth_mesh/Model/Globe.obj");
	earth->setLocalTransform(GeometricTransformation(glm::vec3{150, 0, 200}, glm::quat(glm::vec3{0, 0, 0}), glm::vec3{7, 7, 7}).toMatrix());
	earth->addLocalTransformKeyframe(getGT(glm::vec3{150, 0, 200}, glm::vec3{0, 0, 0}, glm::vec3{7, 7, 7}), 0.5f);
	earth->addLocalTransformKeyframe(getGT(glm::vec3{150, 0, 200}, glm::vec3{0, -3.14/32, 0}, glm::vec3{7, 7, 7}), 3.0f);

	viewer.addRenderable(earth);

	ufoToEarth(viewer, ufo, timeStamp);

	viewer.startAnimation();
}

void ufoToSpace(Viewer& viewer, ShaderProgramPtr& shader, ShaderProgramPtr& textureReflectSkyboxShader, float& timeStamp)
{
	loadSpaceSkybox(viewer);

	glm::vec3 camPos{ 212.17, 64.41, -5.02 };
	glm::vec3 camForward{ -0.73, 0.04, 0.68 };
	glm::vec3 camUp{ -0.02, 0.99, -0.09 };

	viewer.getCamera().setViewMatrix(glm::lookAt(
		camPos,
		camPos + camForward,
		camUp));

	//viewer.getCamera().setPosition(camPos);
	//viewer.getCamera().setForward(camForward);
	//viewer.getCamera().setRight(glm::normalize(glm::cross(camPos, camForward)));

	KeyframedMeshRenderablePtr ufo = std::make_shared<KeyframedMeshRenderable>(textureReflectSkyboxShader, "../../sfmlGraphicsPipeline/meshes/UFO/UFO_COLORED.obj");
	ufo->setLocalTransform(GeometricTransformation(glm::vec3{ 164, 73, -48 }, glm::quat(glm::vec3{ 3.14 / 4, 3.14 / 2, 0 }), glm::vec3{ 10, 10, 10 }).toMatrix());
	viewer.addRenderable(ufo);

	KeyframedMeshRenderablePtr earth = std::make_shared<KeyframedMeshRenderable>(shader, "../../sfmlGraphicsPipeline/meshes/earth_mesh/Model/Globe.obj");
	earth->setLocalTransform(GeometricTransformation(glm::vec3{ 150, 0, 200 }, glm::quat(glm::vec3{ 0, 0, 0 }), glm::vec3{ 7, 7, 7 }).toMatrix());
	earth->addLocalTransformKeyframe(getGT(glm::vec3{ 150, 0, 200 }, glm::vec3{ 0, 0, 0 }, glm::vec3{ 7, 7, 7 }), 0.5f);
	earth->addLocalTransformKeyframe(getGT(glm::vec3{ 150, 0, 200 }, glm::vec3{ 0, -3.14 / 32, 0 }, glm::vec3{ 7, 7, 7 }), 3.0f);

	viewer.addRenderable(earth);

	ufoToSpace(viewer, ufo, timeStamp);

	viewer.startAnimation();
}

void runningAliens(Viewer& viewer, ShaderProgramPtr& shader, float& timeStamp) {

	glm::vec3 camPos{-0.05, 0.57, -1.43};
	glm::vec3 camForward{0.88, -0.46, 0.09};
	glm::vec3 camUp{0, 1, 0};

	viewer.getCamera().setViewMatrix(glm::lookAt(
		camPos,
		camPos + camForward,
		camUp));

	KeyframedMeshRenderablePtr corridor = std::make_shared<KeyframedMeshRenderable>(shader, "../../sfmlGraphicsPipeline/meshes/Corridor/corridor_half.obj");
	//corridor->setLocalTransform(GeometricTransformation(glm::vec3{30, -1, 0}, glm::quat(glm::vec3{0,0,0}), glm::vec3{1, 1, 1}).toMatrix());
	corridor->setLocalTransform(GeometricTransformation(glm::vec3{0, -0.5, 0}, glm::quat(glm::vec3{0, 0, 0}), glm::vec3{1, 1, 1}).toMatrix());
	viewer.addRenderable(corridor);

	glm::mat4 localTransformation;
	glm::vec3 lightPosition(0, 1, 0);
	glm::vec3 d_direction = glm::normalize(glm::vec3(0, 1, 0));
	glm::vec3 d_ambient(0.3, 0.3, 0.3), d_diffuse(0.75, 0.75, 0.75), d_specular(0.6, 0.6, 0.6);
	DirectionalLightPtr directionalLight = std::make_shared<DirectionalLight>(d_direction, d_ambient, d_diffuse, d_specular);
	viewer.setDirectionalLight(directionalLight);

	float p_constant = 1.0, p_linear = 5e-1, p_quadratic = 0.0;

	for (int i = 0; i < 32; i += 8)
	{
		viewer.addPointLight(std::make_shared<PointLight>(glm::vec3(i, 2, 0), d_ambient, d_diffuse, d_specular, p_constant, p_linear, p_quadratic));
		//viewer.addPointLight(std::make_shared<PointLight>(glm::vec3(i, 2, -2), d_ambient, d_diffuse, d_specular, p_constant, p_linear, p_quadratic));
		//viewer.addPointLight(std::make_shared<PointLight>(glm::vec3(i, 2, 2), d_ambient, d_diffuse, d_specular, p_constant, p_linear, p_quadratic));
	}

	KeyframedMeshRenderablePtr leg1 = std::make_shared<KeyframedMeshRenderable>(shader, "../../sfmlGraphicsPipeline/meshes/AlienLeg/AlienLeg.obj");
//	KeyframedMeshRenderablePtr leg2 = std::make_shared<KeyframedMeshRenderable>(shader, "../../sfmlGraphicsPipeline/meshes/AlienLeg/AlienLeg.obj");
//	KeyframedMeshRenderablePtr leg3 = std::make_shared<KeyframedMeshRenderable>(shader, "../../sfmlGraphicsPipeline/meshes/AlienLeg/AlienLeg.obj");
//	KeyframedMeshRenderablePtr leg4 = std::make_shared<KeyframedMeshRenderable>(shader, "../../sfmlGraphicsPipeline/meshes/AlienLeg/AlienLeg.obj");
	KeyframedMeshRenderablePtr leg2 = std::make_shared<KeyframedMeshRenderable>(*leg1);
	KeyframedMeshRenderablePtr leg3 = std::make_shared<KeyframedMeshRenderable>(*leg1);
	KeyframedMeshRenderablePtr leg4 = std::make_shared<KeyframedMeshRenderable>(*leg1);

	KeyframedMeshRenderablePtr legs1[] = {leg1, leg2, leg3, leg4};
	int i = 0;
	for (i = 0; i < 4; i++) {
		viewer.addRenderable(legs1[i]);
		legs1[i]->setLocalTransform(GeometricTransformation(glm::vec3{ i - 3.25f + (i+1 % 2) * 0.75f,0,0}, glm::quat(glm::vec3{ 0,M_PI / 2,-M_PI / 4 }), glm::vec3{ 1,1,1 }).toMatrix());
	}
	runningAliens(viewer, legs1, timeStamp);

	KeyframedMeshRenderablePtr leg5 = std::make_shared<KeyframedMeshRenderable>(shader, "../../sfmlGraphicsPipeline/meshes/AlienLeg/AlienLeg.obj");
	//KeyframedMeshRenderablePtr leg6 = std::make_shared<KeyframedMeshRenderable>(shader, "../../sfmlGraphicsPipeline/meshes/AlienLeg/AlienLeg.obj");
	//KeyframedMeshRenderablePtr leg7 = std::make_shared<KeyframedMeshRenderable>(shader, "../../sfmlGraphicsPipeline/meshes/AlienLeg/AlienLeg.obj");
	//KeyframedMeshRenderablePtr leg8 = std::make_shared<KeyframedMeshRenderable>(shader, "../../sfmlGraphicsPipeline/meshes/AlienLeg/AlienLeg.obj");
	KeyframedMeshRenderablePtr leg6 = std::make_shared<KeyframedMeshRenderable>(*leg5);
	KeyframedMeshRenderablePtr leg7 = std::make_shared<KeyframedMeshRenderable>(*leg5);
	KeyframedMeshRenderablePtr leg8 = std::make_shared<KeyframedMeshRenderable>(*leg5);

	KeyframedMeshRenderablePtr legs2[] = { leg5, leg6, leg7, leg8 };
	for (i = 0; i < 4; i++) {
		viewer.addRenderable(legs2[i]);
		legs2[i]->setLocalTransform(GeometricTransformation(glm::vec3{ i - 3 + (i+1 % 2) * 0.75f,0,0.5f }, glm::quat(glm::vec3{ 0,M_PI / 2,-M_PI / 4 }), glm::vec3{ 1,1,1 }).toMatrix());
	}
	float temp = timeStamp + 0.31f;
	runningAliens(viewer, legs2, temp);

	viewer.startAnimation();
}

void docking(Viewer& viewer, ShaderProgramPtr& shader, float& timeStamp) {
	loadSpaceSkybox(viewer);

	glm::mat4 localTransformation;
	glm::vec3 lightPosition(0, 1, 0);
	glm::vec3 d_direction = glm::normalize(glm::vec3(0, 1, 0));
	glm::vec3 d_ambient(0.3, 0.3, 0.3), d_diffuse(0.75, 0.75, 0.75), d_specular(0.6, 0.6, 0.6);
	DirectionalLightPtr directionalLight = std::make_shared<DirectionalLight>(d_direction, d_ambient, d_diffuse, d_specular);
	viewer.setDirectionalLight(directionalLight);

	glm::vec3 camPos{ -15.0, 5.79, -2.11 };
	glm::vec3 camForward{ 0.88, -0.36, 0.31 };
	glm::vec3 camUp{ 0.35, 0.93, 0.096 };

	viewer.getCamera().setViewMatrix(glm::lookAt(
		camPos,
		camPos + camForward,
		camUp));

	float p_constant = 1.0, p_linear = 5e-1, p_quadratic = 0.0;

	for(int i=0; i < 200; i+=20) {
		viewer.addPointLight(std::make_shared<PointLight>(glm::vec3(i, 3, 0), d_ambient, d_diffuse, d_specular, p_constant, p_linear, p_quadratic));
		//viewer.addPointLight(std::make_shared<PointLight>(glm::vec3(i, 3, -20), d_ambient, d_diffuse, d_specular, p_constant, p_linear, p_quadratic));
		//viewer.addPointLight(std::make_shared<PointLight>(glm::vec3(i, 3, 20), d_ambient, d_diffuse, d_specular, p_constant, p_linear, p_quadratic));
	}

	//Object instanciations, adding them to viewer and creating a hierarchy
	KeyframedMeshRenderablePtr dock = std::make_shared<KeyframedMeshRenderable>(shader, "../../sfmlGraphicsPipeline/meshes/Corridor/corridor_half.obj");
	dock->setLocalTransform(GeometricTransformation(glm::vec3{ 0,0,0 }, glm::quat(glm::vec3{ 0,M_PI,0 }), glm::vec3{ 2,2,2 }).toMatrix());

	KeyframedMeshRenderablePtr ufo = std::make_shared<KeyframedMeshRenderable>(shader, "../../sfmlGraphicsPipeline/meshes/UFO/UFO_COLORED.obj");
	ufo->setLocalTransform(GeometricTransformation(glm::vec3{ 0,0,0 }, glm::quat(glm::vec3{ 0,0,0 }), glm::vec3{ 4,4,4 }).toMatrix());

	viewer.addRenderable(dock);
	viewer.addRenderable(ufo);
	KeyframedMeshRenderable::addChild(dock, ufo);

	//========= Animation =========//

	docking(viewer, dock, ufo, timeStamp);
	viewer.startAnimation();
}

void cowStage(Viewer& viewer, ShaderProgramPtr& shader, float& timeStamp) {

	loadSpaceSkybox(viewer);

	TexturedLightedMeshRenderablePtr surrounding = std::make_shared<TexturedLightedMeshRenderable>(shader, "../../sfmlGraphicsPipeline/meshes/Stage/BirthdayAreaClean.obj");
	surrounding->setLocalTransform(GeometricTransformation(glm::vec3{ 0, 5, 2 }, glm::quat(glm::vec3{ 0, 0, 0 }), glm::vec3{ 3, 3, 3 }).toMatrix());
	viewer.addRenderable(surrounding);

	TexturedLightedMeshRenderablePtr stage = std::make_shared<TexturedLightedMeshRenderable>(shader, "../../sfmlGraphicsPipeline/meshes/WoodenFloor/ChallengeStage/challenge_stage.obj");
	stage->setLocalTransform(GeometricTransformation(glm::vec3{ 0, 0, -1 }, glm::quat(glm::vec3{ 0, 0, 0 }), glm::vec3{ 7, 7, 7 }).toMatrix());
	viewer.addRenderable(stage);

	glm::mat4 localTransformation;
	glm::vec3 lightPosition(0, 1, 0);
	glm::vec3 d_direction = glm::normalize(glm::vec3(0, 1, 0));
	glm::vec3 d_ambient(0.3, 0.3, 0.3), d_diffuse(0.75, 0.75, 0.75), d_specular(0.6, 0.6, 0.6);
	DirectionalLightPtr directionalLight = std::make_shared<DirectionalLight>(d_direction, d_ambient, d_diffuse, d_specular);
	viewer.setDirectionalLight(directionalLight);

	glm::vec3 camPos{ 0, 10, 13 };
	glm::vec3 camForward{ 0 ,-0.25f, -0.75f };
	glm::vec3 camUp{ 0, 0.75f, -0.25f };

	viewer.getCamera().setViewMatrix(glm::lookAt(
		camPos,
		camPos + camForward,
		camUp));


	glm::vec3 cowStartPos = { 0,5,0 };

	KeyframedMeshRenderablePtr cow = std::make_shared<KeyframedMeshRenderable>(shader, "../../sfmlGraphicsPipeline/meshes/cow/cow.obj");
	cow->setLocalTransform(GeometricTransformation(cowStartPos, glm::quat(glm::vec3{ 0,0,0 }), glm::vec3{ 0.1,0.1,0.1 }).toMatrix());

	viewer.addRenderable(cow);

}


//Animations
void ufoOutOfMotherShip(Viewer& viewer, KeyframedMeshRenderablePtr& motherShip, KeyframedMeshRenderablePtr& ufo, float& timeStamp) {
	//Phase 1 : global translation
	GeometricTransformation gt1 = getGT(glm::vec3{ 0,0,0 }, glm::vec3{ 0,0,0 }, glm::vec3{ 1,1,1 });
	GeometricTransformation gt2 = getGT(glm::vec3{ 5,0,0 }, glm::vec3{ 0,0,0 }, glm::vec3{ 1,1,1 });
	motherShip->addParentTransformKeyframe(gt1, timeStamp + 0.0f);
	motherShip->addParentTransformKeyframe(gt2, timeStamp + 3.0f);

	//Phase 2_a : local translation of motherShip
	gt1 = getGT(glm::vec3{ 0,0,0 }, glm::vec3{ 0,0,0 }, glm::vec3{ 1,1,1 });
	gt2 = getGT(glm::vec3{ 5,0,0 }, glm::vec3{ 0,0,0 }, glm::vec3{ 1,1,1 });
	motherShip->addLocalTransformKeyframe(gt1, timeStamp + 3.0f);
	motherShip->addLocalTransformKeyframe(gt2, timeStamp + 6.0f);

	//Phase 2_b : local movement of UFO
	gt1 = getGT(glm::vec3{ 0,-0.2f,0 }, glm::vec3{ 0,0,0 }, glm::vec3{ 0.05,0.05,0.05 });
	gt2 = getGT(glm::vec3{ 0,-0.8f,-0.5f }, glm::vec3{ 0,0,0 }, glm::vec3{ 0.05,0.05,0.05 });
	GeometricTransformation gt3 = getGT(glm::vec3{ 0,-0.9f,-1.0f }, glm::vec3{ 0,0,0 }, glm::vec3{ 0.05,0.05,0.05 });
	GeometricTransformation gt4 = getGT(glm::vec3{ 0,-0.6f,-2.0f }, glm::vec3{ 0,0,0 }, glm::vec3{ 0.05,0.05,0.05 });
	GeometricTransformation gt5 = getGT(glm::vec3{ 0,0.1f,-3.5f }, glm::vec3{ 0,0,0 }, glm::vec3{ 0.05,0.05,0.05 });
	GeometricTransformation gt6 = getGT(glm::vec3{ 0,0.9f,-5 }, glm::vec3{ 0,0,0 }, glm::vec3{ 0.05,0.05,0.05 });
	ufo->addLocalTransformKeyframe(gt1, timeStamp + 3.0f);
	ufo->addLocalTransformKeyframe(gt2, timeStamp + 3.5f);
	ufo->addLocalTransformKeyframe(gt3, timeStamp + 4.0f);
	ufo->addLocalTransformKeyframe(gt4, timeStamp + 4.5f);
	ufo->addLocalTransformKeyframe(gt5, timeStamp + 5.0f);
	ufo->addLocalTransformKeyframe(gt6, timeStamp + 5.5f);

	viewer.setAnimationLoop(true, 6.5f);
}

void OutOfLightSpeed(Viewer &viewer, KeyframedMeshRenderablePtr &spaceShip, float& timeStamp)
{
	GeometricTransformation gt1 = getGT(glm::vec3{ 0,0,0 }, glm::vec3{ 0,0,0 }, glm::vec3{ 0,0,0 });
	GeometricTransformation gt2 = getGT(glm::vec3{ 20,0,0 }, glm::vec3{ 0,0,0 }, glm::vec3{ 5,0.4,0.4 });
	GeometricTransformation gt3 = getGT(glm::vec3{ 21,0,0 }, glm::vec3{ 0,0,0 }, glm::vec3{ 5,5,5 });
	GeometricTransformation gt4 = getGT(glm::vec3{ 35,0,0 }, glm::vec3{ 0,0,0 }, glm::vec3{ 5,5,5 });
	spaceShip->addLocalTransformKeyframe(gt1, timeStamp + 0.0f);
	spaceShip->addLocalTransformKeyframe(gt1, timeStamp + 3.0f);
	spaceShip->addLocalTransformKeyframe(gt2, timeStamp + 3.1f);
	spaceShip->addLocalTransformKeyframe(gt3, timeStamp + 3.2f);
	spaceShip->addLocalTransformKeyframe(gt4, timeStamp + 7.0f);

	viewer.setAnimationLoop(true, 7.0f);
}

void ufoToEarth(Viewer& viewer, KeyframedMeshRenderablePtr& ufo, float& timeStamp)
{
	GeometricTransformation gt1 = getGT(glm::vec3{ 164, 73, -48 }, glm::vec3{ 0, 0, 0 }, glm::vec3{ 10, 10, 10 });
	GeometricTransformation gt2 = getGT(glm::vec3{ 130, 43, 163 }, glm::vec3{ 0, -3.14, -3.14 / 30 }, glm::vec3{ 0.0001f, 0.0001f, 0.0001f });
	ufo->addLocalTransformKeyframe(gt1, timeStamp + 0.5f);
	ufo->addLocalTransformKeyframe(gt2, timeStamp + 3.0f);

	viewer.setAnimationLoop(true, 3.0f);
}

void ufoToSpace(Viewer& viewer, KeyframedMeshRenderablePtr& ufo, float& timeStamp)
{
	GeometricTransformation gt1 = getGT(glm::vec3{ 164, 73, -48 }, glm::vec3{ 0, 0, 0 }, glm::vec3{ 10, 10, 10 });
	GeometricTransformation gt2 = getGT(glm::vec3{ 130, 43, 163 }, glm::vec3{ 0, -3.14, -3.14 / 30 }, glm::vec3{ 0.0001f, 0.0001f, 0.0001f });
	ufo->addLocalTransformKeyframe(gt2, timeStamp + 0.5f);
	ufo->addLocalTransformKeyframe(gt1, timeStamp + 3.0f);

	viewer.setAnimationLoop(true, 3.0f);
}

void runningAliens(Viewer& viewer, KeyframedMeshRenderablePtr legs[], float& timeStamp) {
	
	float delayLoc = timeStamp;
	int i = 0;
	for (i = 0;i<4;i++) {
		makeStep(viewer, legs[i], glm::vec3{ 0.0f,0.0f,0.0f }, delayLoc);
		delayLoc += 0.155f;
	}
	for (i = 0; i < 4; i++) {
		makeStep(viewer, legs[i], glm::vec3{ 1.0f,0.0f,0.0f }, delayLoc);
		delayLoc += 0.155f;
	}
	for (i = 0; i < 4; i++) {
		makeStep(viewer, legs[i], glm::vec3{ 2.0f,0.0f,0.0f }, delayLoc);
		delayLoc += 0.155f;
	}
	
	viewer.setAnimationLoop(true, delayLoc+1.0f);
}

void makeStep(Viewer& viewer, KeyframedMeshRenderablePtr leg, glm::vec3 offset, float& timeStamp) {

	glm::mat4 local = leg->getLocalTransform();
	GeometricTransformation gt1 = getGT(glm::vec3{ local[3][0] + 0 + offset.x, local[3][1] + 0 + offset.y, local[3][2] + 0 + offset.z }, glm::vec3{ 0,M_PI / 2,-M_PI / 4 }, glm::vec3{ 1,1,1 });
	GeometricTransformation gt2 = getGT(glm::vec3{ local[3][0] + 0.5f + offset.x, local[3][1] + 0.6f + offset.y, local[3][2] + 0 + offset.z }, glm::vec3{ 0,M_PI / 2,-M_PI / 3 }, glm::vec3{ 1,1,1 });
	GeometricTransformation gt3 = getGT(glm::vec3{ local[3][0] + 0.7f + offset.x, local[3][1] + 0.3f + offset.y, local[3][2] + 0 + offset.z }, glm::vec3{ 0,M_PI / 2,-M_PI / 3.5f }, glm::vec3{ 1,1,1 });
	GeometricTransformation gt4 = getGT(glm::vec3{ local[3][0] + 1 + offset.x, local[3][1] + 0 + offset.y, local[3][2] + 0 + offset.z }, glm::vec3{ 0,M_PI / 2,-M_PI / 4 }, glm::vec3{ 1,1,1 });
	leg->addLocalTransformKeyframe(gt1, timeStamp +0.0f);
	leg->addLocalTransformKeyframe(gt2, timeStamp +0.1f);
	leg->addLocalTransformKeyframe(gt3, timeStamp +0.2f);
	leg->addLocalTransformKeyframe(gt4, timeStamp +0.3f);

}

void docking(Viewer& viewer, KeyframedMeshRenderablePtr& dock, KeyframedMeshRenderablePtr& ufo, float& timeStamp) {
	//Phase 1 : global translation
	GeometricTransformation gt1 = getGT(glm::vec3{ 0,0,0 }, glm::vec3{ 0,0,0 }, glm::vec3{ 1,1,1 });
	GeometricTransformation gt2 = getGT(glm::vec3{ -5,0,0 }, glm::vec3{ 0,0,0 }, glm::vec3{ 1,1,1 });
	//dock->addParentTransformKeyframe(gt1, timeStamp + 0.0f);
	//dock->addParentTransformKeyframe(gt2, timeStamp + 1.0f);

	//Phase 2_a : local translation of motherShip
	gt1 = getGT(glm::vec3{ 0,0,0 }, glm::vec3{ 0,M_PI,0 }, glm::vec3{ 2,2,2 });
	gt2 = getGT(glm::vec3{ -15,0,0 }, glm::vec3{ 0,M_PI,0 }, glm::vec3{ 2,2,2 });
	//dock->addLocalTransformKeyframe(gt1, timeStamp + 1.0f);
	//dock->addLocalTransformKeyframe(gt2, timeStamp + 4.0f);

	//Phase 2_b : local movement of UFO
	gt1 = getGT(glm::vec3{ 0, 1.0f,0 }, glm::vec3{ 0,0,0 }, glm::vec3{ 1,1,1 });
	gt2 = getGT(glm::vec3{ 5.0f, 1.0f,0.0f }, glm::vec3{ 0,0,0 }, glm::vec3{ 1,1,1 });
	GeometricTransformation gt3 = getGT(glm::vec3{ 10.0f,1.0f,0.0f }, glm::vec3{ 0,0,0 }, glm::vec3{ 1,1,1 });
	GeometricTransformation gt4 = getGT(glm::vec3{ 15.0f,-1.0f,0.0f }, glm::vec3{ 0,0,-3.1415 / 20 }, glm::vec3{ 1,1,1 });
	GeometricTransformation gt5 = getGT(glm::vec3{ 20.0f,-3.0f,-1.5f }, glm::vec3{ 0,0,-3.1415 / 15 }, glm::vec3{ 1,1,1 });
	GeometricTransformation gt6 = getGT(glm::vec3{ 25.0f,-7.0f,-3.0f}, glm::vec3{ 0,0,-3.1415 / 10 }, glm::vec3{ 1,1,1 });
	ufo->addLocalTransformKeyframe(gt1, timeStamp + 1.0f);
	ufo->addLocalTransformKeyframe(gt2, timeStamp + 1.5f);
	ufo->addLocalTransformKeyframe(gt3, timeStamp + 2.0f);
	ufo->addLocalTransformKeyframe(gt4, timeStamp + 2.5f);
	ufo->addLocalTransformKeyframe(gt5, timeStamp + 3.0f);
	ufo->addLocalTransformKeyframe(gt6, timeStamp + 3.5f);

	viewer.setAnimationLoop(true, timeStamp + 4.5f);
}
*/