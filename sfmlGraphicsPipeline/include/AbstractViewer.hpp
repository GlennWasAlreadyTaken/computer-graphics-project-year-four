#ifndef ABSTRACT_VIEWER_HPP
#define ABSTRACT_VIEWER_HPP

#include <GL/glew.h>
#include <SFML/Graphics.hpp>
#include <SFML/OpenGL.hpp>
#include "Camera.hpp"
#include "Renderable.hpp"
#include "lighting/Light.hpp"
#include "texturing/SkyboxRenderable.hpp"
#include "SceneStorage.hpp"

class AbstractViewer {

public:
    inline AbstractViewer(float width, float height) : m_window{
                                                    sf::VideoMode(width, height),
                                                    "Computer Graphics Practicals",
                                                    sf::Style::Default,
                                                    sf::ContextSettings{24 /* depth*/, 8 /*stencil*/, 4 /*anti aliasing level*/, 4 /*GL major version*/, 0 /*GL minor version*/}}
    {}

    /**@brief Get the camera.
     *
     * Access to the camera used to render the scene in the viewer.
     * @return A reference to the viewer's camera. */
    inline Camera &getCamera() {return m_camera; };

    /**@brief Get the window.
     *
     * Access to the window used to render the scene in the viewer.
     * @return A reference to the viewer's window. */

    inline sf::RenderWindow &getWindow() {return m_window; };

    /**
     * \brief addRenderable
     *
     * Add a renderable to the set of renderabbles \ref m_renderables of the viewer.
     * \param r A renderable to add to \ref m_renderables.
     */
    virtual void addRenderable(int sceneIndex, RenderablePtr r) = 0;

    inline const std::unordered_set<RenderablePtr> &getRenderables() { return sceneStorage.renderables(); }

    inline void setDirectionalLight(int sceneIndex, const DirectionalLightPtr &directionalLight) { sceneStorage.addDirectionalLight(sceneIndex, directionalLight); };

    inline void addPointLight(int sceneIndex, const PointLightPtr &pointLight) { sceneStorage.addPointLight(sceneIndex, pointLight); };

    inline void addSpotLight(int sceneIndex, const SpotLightPtr &spotLight) { sceneStorage.addSpotLight(sceneIndex, spotLight); };

    /** \brief Set the skybox of the scene.
     * 
     */
    inline void setSkybox(int sceneIndex, const SkyboxRenderablePtr &skybox) { sceneStorage.addSkybox(sceneIndex, skybox); };

    inline void setNumberOfScenes(int nbScenes) { sceneStorage.setNumberOfScenes(nbScenes); }

protected:
    Camera m_camera; /*!< Camera used to render the scene in the Viewer. */

    sf::RenderWindow m_window;                       /*!< Pointer to the render window. */

    SceneStorage sceneStorage;                       /* Stores Renderables, Lights, etc */
};

#endif // ABSTRACT_VIEWER_HPP