#ifndef GUI_HPP
#define GUI_HPP

#include "AbstractViewer.hpp"
#include <SFML/Graphics.hpp>
#include <SFML/OpenGL.hpp>

class GUI {

public:
    GUI(AbstractViewer& viewer);
    void update();
    void processEvent(sf::Event event);
    void close();
private:

    sf::Clock m_deltaClock;
    AbstractViewer& m_viewer;

    void display();
    void displayRenderablesMenu();
};

#endif // GUI_HPP