/**@file
 * @brief Define a camera transformation.
 *
 * This file defines a camera transformation, with orientation, translation
 * and scale.
 */
#ifndef CAMERATRANSFORMATION_HPP_
#define CAMERATRANSFORMATION_HPP_

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/compatibility.hpp>

/**@brief Describe a camera transformation.
 *
 * This class is used to define a camera transformation that will be
 * interpolated between key frames.
 */
class CameraTransformation
{
public:
    /**@brief Instance construction
   *
   * Construct a camera transformation from a translation, a rotation and
   * a scale. The result is similar to a transformation matrix given by the
   * matricial product: translation * rotation * scale.
   */
    inline CameraTransformation(
        glm::vec3 position = glm::vec3{0, 0, 0},
        glm::vec3 forward = glm::vec3{1, 0, 0},
        glm::vec3 up = glm::vec3{0, 1, 0}) 
    : m_position(position), m_forward(forward), m_up(up) 
    {};

    /**@brief Convert to a transformation matrix.
   *
   * Get a transformation matrix that represent this camera transformation. */
    inline glm::mat4 toMatrix() const
    {
        return glm::lookAt(m_position, m_position + m_forward, m_up);
    };

    /**@brief Set the position component.
   *
   * Set the position of this transformation.
   * @param position The new position. */
    inline void setPosition(const glm::vec3 &position) 
    {
        m_position = position; 
    };

    /**@brief Get the position component.
   *
   * Get the position component of this transformation.
   * @return The position.
   */
    inline const glm::vec3 &getPosition() const 
    { 
        return m_position; 
    };


    /**@brief Set the forward component.
   *
   * Set the forward of this transformation.
   * @param forward The new forward. */
    inline void setForward(const glm::vec3 &forward)
    {
        m_forward = forward;
    };

    /**@brief Get the forward component.
   *
   * Get the forward component of this transformation.
   * @return The forward.
   */
    inline const glm::vec3 &getForward() const
    {
        return m_forward;
    };


    /**@brief Set the up component.
   *
   * Set the up of this transformation.
   * @param up The new up. */
    inline void setUp(const glm::vec3 &up)
    {
        m_up = up;
    };

    /**@brief Get the up component.
   *
   * Get the up component of this transformation.
   * @return The up.
   */
    inline const glm::vec3 &getUp() const
    {
        return m_forward;
    };

    inline static CameraTransformation interpolate(const CameraTransformation &g1, const CameraTransformation &g2, float factor)
    {
        glm::vec3 position = glm::lerp(g1.getPosition(), g2.getPosition(), factor);
        glm::vec3 forward = glm::lerp(g1.getForward(), g2.getForward(), factor);
        glm::vec3 up = glm::lerp(g1.getUp(), g2.getUp(), factor);
        return CameraTransformation(position, forward, up);
    }

private:
    glm::vec3 m_position;
    glm::vec3 m_forward;
    glm::vec3 m_up;
};

#endif
