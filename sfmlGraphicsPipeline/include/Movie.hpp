#ifndef MOVIE_HPP
#define MOVIE_HPP

#include "./../include/Viewer.hpp"
#include <KeyframedMeshRenderable.hpp>

class Movie 
{
public:

    Movie(Viewer &viewer);
    void start();

private:
    // -------------------------
    // Attributes
    // -------------------------
    Viewer& m_viewer;

    // ------------
    // Shaders
    // ------------
    ShaderProgramPtr m_skyboxShader;
    ShaderProgramPtr m_textureShader;
    ShaderProgramPtr m_textureReflectSkyboxShader;
    ShaderProgramPtr m_flatShader;

    // ------------
    // Skyboxes
    // ------------
    SkyboxRenderablePtr m_spaceSkybox;
    SkyboxRenderablePtr m_earthSkybox;

    // ------------
    // Renderables
    // ------------
    KeyframedMeshRenderablePtr m_spaceship;
    KeyframedMeshRenderablePtr m_ufo;
    KeyframedMeshRenderablePtr m_earth;
    KeyframedMeshRenderablePtr m_cow;
    KeyframedMeshRenderablePtr m_alienLeg;
    KeyframedMeshRenderablePtr m_hill;
    KeyframedMeshRenderablePtr m_corridor;
    KeyframedMeshRenderablePtr m_surrounding;
    KeyframedMeshRenderablePtr m_stage;

    // -------------------------
    // Methods
    // -------------------------
    void loadShaders();
    void loadRenderables();
    void loadSkyboxes();
    void loadScenes();

    // ------------
    // Skyboxes
    // ------------
    void loadSpaceSkybox();
    void setSpaceSkybox(int sceneIndex);

    void loadEarthSkybox();
    void setEarthSkybox(int sceneIndex);

    // ------------
    // Scenes
    // ------------
    void outOfLightSpeed(int sceneNumber);
    void runningAliens(int sceneNumber);
    void ufoOutOfMotherShip(int sceneNumber);
    void cowAbducted(int sceneNumber);
	void cowStage(int sceneNumber);
	void ufoToEarth(int sceneNumber);
	void ufoToSpace(int sceneNumber);
	void docking(int sceneNumber);

    // ------------
    // Animations
    // ------------
    float runningAliensAnimation(KeyframedMeshRenderablePtr* legs, float delay);
    void makeStep(KeyframedMeshRenderablePtr leg, float delay, glm::vec3 offset);

    // ------------
    //
    // ------------
};

#endif // MOVIE_HPP