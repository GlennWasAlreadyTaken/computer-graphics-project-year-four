#ifndef IMAGE_HPP
#define IMAGE_HPP

//#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

#include <string>

class Image
{
public:
    /** /brief Returns 0 if the image was not loaded, or the format number if it was correctly loaded.
    * 
    */
    bool load(std::string filename, bool flipVertically = true);

    ~Image();

    unsigned char * data();
    int width();
    int height();
    int nrChannels();

private:
    int m_width, m_height, m_nrChannels;
    unsigned char * m_data;

};

#endif // IMAGE_HPP