#ifndef SCENESTORAGE_HPP
#define SCENESTORAGE_HPP

#include <vector>
#include <set>

#include "Renderable.hpp"
#include "lighting/Light.hpp"
#include "texturing/SkyboxRenderable.hpp"

class SceneStorage {

public:
    inline SceneStorage()
    {
        m_currentIndex = 0;
    };

    inline std::unordered_set<RenderablePtr>& renderables()
    {
        return m_renderables[m_currentIndex];
    };

    inline void addRenderable(int index, const RenderablePtr& r)
    {
        m_renderables[index].insert(r);
    };

    inline DirectionalLightPtr& directionalLight()
    {
        return m_directionalLight[m_currentIndex];
    };

    inline void addDirectionalLight(int index, const DirectionalLightPtr& r)
    {
        m_directionalLight.insert(m_directionalLight.begin() + index, r);
    };

    inline std::vector<PointLightPtr>& pointLights()
    {
        return m_pointLights[m_currentIndex];
    };

    inline void addPointLight(int index, const PointLightPtr& r)
    {
        m_pointLights[index].push_back(r);
    };

    inline std::vector<SpotLightPtr>& spotLights()
    {
        return m_spotLights[m_currentIndex];
    };

    inline void addSpotLight(int index, const SpotLightPtr& r)
    {
        m_spotLights[index].push_back(r);
    };

    inline SkyboxRenderablePtr& skyboxRenderable()
    {
        return m_skyboxRenderable[m_currentIndex];
    };

    inline void addSkybox(int index, const SkyboxRenderablePtr& r)
    {
        m_skyboxRenderable.insert(m_skyboxRenderable.begin() + index, r);
    };

    inline glm::mat4& viewMatrix()
    {
        return m_viewMatrices[m_currentIndex];
    };

    inline void addViewMatrix(int index, const glm::mat4 &r)
    {
        m_viewMatrices.insert(m_viewMatrices.begin() + index, r);
    };

    inline float animationDuration()
    {
        return m_animationDuration[m_currentIndex];
    };

    inline void addAnimationDuration(int index, const float &r)
    {
        m_animationDuration.insert(m_animationDuration.begin() + index, r);
    };

    inline void nextScene() {
        if(m_currentIndex < m_nbOfScenes - 1)
            m_currentIndex++;
        else    //TODO : add a parameter loop for that ?
            m_currentIndex = 0;
    };

    inline void setNumberOfScenes(int nbScenes) {
        m_nbOfScenes = nbScenes;
        m_renderables.resize(nbScenes);
        m_directionalLight.resize(nbScenes);
        m_pointLights.resize(nbScenes);
        m_spotLights.resize(nbScenes);
        m_skyboxRenderable.resize(nbScenes);
    }

    inline int nbOfScenes() {return m_nbOfScenes;}
private:

    int m_currentIndex;
    int m_nbOfScenes;

    std::vector<std::unordered_set<RenderablePtr>> m_renderables; /*!< Set of renderables that the viewer displays. */
    std::vector<DirectionalLightPtr> m_directionalLight;          /*!< Pointer to a directional light. */
    std::vector<std::vector<PointLightPtr>> m_pointLights;       /*!< Vector of pointer to the point lights. */
    std::vector<std::vector<SpotLightPtr>> m_spotLights;          /*!< Vector of pointer to the spot lights. */
    std::vector<SkyboxRenderablePtr> m_skyboxRenderable;           /* pointer to the skybox renderable. */
    std::vector<glm::mat4> m_viewMatrices;           
    std::vector<float> m_animationDuration;           
};

#endif // SCENESTORAGE_HPP