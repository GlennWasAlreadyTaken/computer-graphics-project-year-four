#ifndef SKYBOX_RENDERABLE_HPP
#define SKYBOX_RENDERABLE_HPP

#include "./../HierarchicalRenderable.hpp"
#include "./../lighting/Material.hpp"
#include <vector>
#include <glm/glm.hpp>

class SkyboxRenderable : public HierarchicalRenderable
{
public :
    ~SkyboxRenderable();
    SkyboxRenderable(ShaderProgramPtr shaderProgram, const std::vector<std::string>& faces);
    void setMaterial(const MaterialPtr& material);

    void bindCubemap(ShaderProgramPtr& shader);
    void releaseCubemap();

private:
    void do_draw();
    void do_animate( float time );
    void loadCubemap(std::vector<std::string> faces);

    std::vector< glm::vec3 > m_positions;

    unsigned int m_pBuffer;

    unsigned int textureID;
};

typedef std::shared_ptr<SkyboxRenderable> SkyboxRenderablePtr;

#endif // SKYBOX_RENDERABLE_HPP
