#ifndef IO_HPP
#define IO_HPP

/**@file
 *@brief Input/Output functions.
 *
 * Currently, this file only contains I/O functions for OBJ meshes.*/

#include <vector>
#include <glm/glm.hpp>
#include <string>
#include <map>
#include "./../include/lighting/Material.hpp"

/**@brief Returns only the path of the folder containing the file.
 * 
 * Returns only the path of the folder containing the given file.
 * e.g.: /bin/foo/test.txt -> returns /bin/foo/
 * 
 * @param s the full path of the file.
 * @return only the path of the folder containing the file.
 */
std::string getPathName(const std::string &s);

typedef std::vector<
            std::map<
                std::pair<unsigned int, unsigned int>, unsigned int
            >
        > 
vToVtVn;

/**
 * Returns the index to use in the global element buffer.
 */
unsigned int getIndex(const unsigned int &localIndex, const unsigned int &texCoords, const unsigned int &normal, const vToVtVn &map, unsigned int &globalIndex);

/**@brief Collect mesh data from an OBJ file.
 *
 * This function opens an OBJ mesh file to collect information such
 * as vertex position, vertex indices of a face, vertex normals and vertex
 * texture coordinates.
 *
 * @param filename The path to the mesh file.
 * @param positions The vertex positions.
 * @param indices The vertex indices of faces.
 * @param normals The vertex normals.
 * @param texcoords The vertex texture coordinates.
 * @return False if import failed, true otherwise.
 */
    bool read_obj(
        const std::string &filename,
        std::vector<glm::vec3> &positions,
        std::vector<unsigned int> &indices,
        std::vector<glm::vec3> &normals,
        std::vector<glm::vec2> &texcoords);

/**@brief Collect mesh data from an OBJ file.
 *
 * This function opens an OBJ mesh file to collect information such
 * as vertex position, vertex indices of a face, vertex normals and vertex
 * texture coordinates.
 *
 * @param filename The path to the mesh file.
 * @param positions The vertex positions.
 * @param normals The vertex normals.
 * @param texcoords The vertex texture coordinates.
 * @param materials the vector of materials of the model.
 * @param indicesPerMaterials the vertices index per material
 * 
 * @return False if import failed, true otherwise.
 */
bool read_obj(
    const std::string &filename,
    std::vector<glm::vec3> &positions,
    std::vector<glm::vec3> &normals,
    std::vector<glm::vec2> &texcoords,
    std::vector<MaterialPtr> &materials,
    std::vector<std::vector<unsigned int>> &indicesPerMaterial);

/**@brief Collect mesh data from an OBJ file.
 *
 * This function opens an OBJ mesh file to collect information such
 * as vertex position, vertex indices of a face, vertex normals and vertex
 * texture coordinates.
 *
 * @param filename The filename of the mesh file.
 * @param path The path to the filename.
 * @param positions The vertex positions.
 * @param indices The vertex indices of faces.
 * @param normals The vertex normals.
 * @param texcoords The vertex texture coordinates.
 * @return False if import failed, true otherwise.
 */
bool read_obj(
    const std::string &filename,
    const std::string &path,
    std::vector<glm::vec3> &positions,
    std::vector<glm::vec3> &normals,
    std::vector<glm::vec2> &texcoords,
    std::vector<MaterialPtr> &materials,
    std::vector<std::vector<unsigned int>> &indicesPerMaterial);

#endif //IO_HPP
