#ifndef KeyframedMeshRenderable_HPP
#define KeyframedMeshRenderable_HPP

#include "GeometricTransformation.hpp"
#include "texturing/TexturedLightedMeshRenderable.hpp"
#include "KeyframeCollection.hpp"

#include <glm/glm.hpp>

class KeyframedMeshRenderable : public TexturedLightedMeshRenderable
{
public:
	~KeyframedMeshRenderable();
	KeyframedMeshRenderable(ShaderProgramPtr program,
							const std::string &mesh_filename);

	/**
	 * \brief Add a keyframe for the local transformation of the renderable.
	 *
	 * Add a keyframe to m_localKeyframes described by a geometric transformation and a time.
	 * \param transformation The geometric transformation of the keyframe.
	 * \param time The time of the keyframe.
	 */
	void addLocalTransformKeyframe(const GeometricTransformation& transformation, float time);

	/**
	 * \brief Add a keyframe for the parent transformation of the renderable.
	 *
	 * Add a keyframe to m_parentKeyframes described by a geometric transformation and a time.
	 * \param transformation The geometric transformation of the keyframe.
	 * \param time The time of the keyframe.
	 */
	void addParentTransformKeyframe(const GeometricTransformation& transformation, float time);

	void do_animate(float time);
	
private:
	
	KeyframeCollection m_localKeyframes; /*!< A collection of keyframes for the local transformation of renderable. */
	KeyframeCollection m_parentKeyframes; /*!< A collection of keyframes for the parent transformation of renderable. */

};

typedef std::shared_ptr<KeyframedMeshRenderable> KeyframedMeshRenderablePtr;

#endif

