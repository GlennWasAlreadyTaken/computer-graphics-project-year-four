#ifndef TEXTURE_HPP
#define TEXTURE_HPP

#include "./../../include/gl_helper.hpp"
#include "./../../include/log.hpp"
#include "./../../include/Utils.hpp"

#include <GL/glew.h>

#include <string>
#include "ShaderProgram.hpp"

class Texture 
{


    public:
        Texture();
        Texture(std::string filename);
        ~Texture();

        void render(const ShaderProgramPtr& shaderPrg, std::string nameInShader, int nbTexture = 0);

        void bind();
        void release(int nbTexture = 0);

        void setWrapOption(int wrapOption);
        void setFilterOption(int filterOption);

        const std::string MISSING_TEXTURE_PATH = "./../../sfmlGraphicsPipeline/textures/missing_texture.png";

    private:
        GLuint m_texId;
        std::string m_texFilename;

    
};

typedef std::shared_ptr<Texture> TexturePtr;

#endif // TEXTURE_HPP
