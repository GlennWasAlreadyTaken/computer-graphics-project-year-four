#version 400

in vec3 surfel_texCoord;
out vec4 outColor;

uniform samplerCube skybox;

void main()
{
    outColor = texture(skybox, surfel_texCoord);
}