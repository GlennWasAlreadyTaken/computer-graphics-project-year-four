#version 400
uniform mat4 projMat, viewMat;

in vec3 vPosition;

out vec3 surfel_texCoord;

void main()
{
    surfel_texCoord = vPosition;
    vec4 pos = projMat * viewMat * vec4(vPosition, 1.0);
    gl_Position = pos.xyww;
}  