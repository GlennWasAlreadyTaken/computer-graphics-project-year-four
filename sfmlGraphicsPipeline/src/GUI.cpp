#include "./../include/GUI.hpp"
#include "./../include/imgui/imgui.h"
#include "./../include/imgui/imgui-SFML.h"

#include <glm/gtx/matrix_decompose.hpp>
#include <glm/gtx/euler_angles.hpp>
#include <glm/gtx/quaternion.hpp>

GUI::GUI(AbstractViewer &viewer) : m_viewer(viewer)
{
    ImGui::SFML::Init(m_viewer.getWindow());
    m_viewer.getWindow().resetGLStates();
    m_viewer.getWindow().pushGLStates();
}

void GUI::update()
{
    m_viewer.getWindow().popGLStates();
    ImGui::SFML::Update(m_viewer.getWindow(), m_deltaClock.restart());

    display();

    ImGui::SFML::Render(m_viewer.getWindow());
    m_viewer.getWindow().pushGLStates();
}

void GUI::processEvent(sf::Event event)
{
    ImGui::SFML::ProcessEvent(event);
}

void GUI::close()
{
    ImGui::SFML::Shutdown();
}

void HelpMarker(std::string) {}

void GUI::display()
{
    if (ImGui::CollapsingHeader("Configuration"))
    {
        ImGuiIO &io = ImGui::GetIO();

        if (ImGui::TreeNode("Configuration##2"))
        {
            ImGui::CheckboxFlags("io.ConfigFlags: NavEnableKeyboard", (unsigned int *)&io.ConfigFlags, ImGuiConfigFlags_NavEnableKeyboard);
            ImGui::CheckboxFlags("io.ConfigFlags: NavEnableGamepad", (unsigned int *)&io.ConfigFlags, ImGuiConfigFlags_NavEnableGamepad);
            ImGui::SameLine();
            HelpMarker("Required back-end to feed in gamepad inputs in io.NavInputs[] and set io.BackendFlags |= ImGuiBackendFlags_HasGamepad.\n\nRead instructions in imgui.cpp for details.");
            ImGui::CheckboxFlags("io.ConfigFlags: NavEnableSetMousePos", (unsigned int *)&io.ConfigFlags, ImGuiConfigFlags_NavEnableSetMousePos);
            ImGui::SameLine();
            HelpMarker("Instruct navigation to move the mouse cursor. See comment for ImGuiConfigFlags_NavEnableSetMousePos.");
            ImGui::CheckboxFlags("io.ConfigFlags: NoMouse", (unsigned int *)&io.ConfigFlags, ImGuiConfigFlags_NoMouse);

            // The "NoMouse" option above can get us stuck with a disable mouse! Provide an alternative way to fix it:
            if (io.ConfigFlags & ImGuiConfigFlags_NoMouse)
            {
                if (fmodf((float)ImGui::GetTime(), 0.40f) < 0.20f)
                {
                    ImGui::SameLine();
                    ImGui::Text("<<PRESS SPACE TO DISABLE>>");
                }
                if (ImGui::IsKeyPressed(ImGui::GetKeyIndex(ImGuiKey_Space)))
                    io.ConfigFlags &= ~ImGuiConfigFlags_NoMouse;
            }
            ImGui::CheckboxFlags("io.ConfigFlags: NoMouseCursorChange", (unsigned int *)&io.ConfigFlags, ImGuiConfigFlags_NoMouseCursorChange);
            ImGui::SameLine();
            HelpMarker("Instruct back-end to not alter mouse cursor shape and visibility.");
            ImGui::Checkbox("io.ConfigInputTextCursorBlink", &io.ConfigInputTextCursorBlink);
            ImGui::SameLine();
            HelpMarker("Set to false to disable blinking cursor, for users who consider it distracting");
            ImGui::Checkbox("io.ConfigWindowsResizeFromEdges", &io.ConfigWindowsResizeFromEdges);
            ImGui::SameLine();
            HelpMarker("Enable resizing of windows from their edges and from the lower-left corner.\nThis requires (io.BackendFlags & ImGuiBackendFlags_HasMouseCursors) because it needs mouse cursor feedback.");
            ImGui::Checkbox("io.ConfigWindowsMoveFromTitleBarOnly", &io.ConfigWindowsMoveFromTitleBarOnly);
            ImGui::Checkbox("io.MouseDrawCursor", &io.MouseDrawCursor);
            ImGui::SameLine();
            HelpMarker("Instruct Dear ImGui to render a mouse cursor itself. Note that a mouse cursor rendered via your application GPU rendering path will feel more laggy than hardware cursor, but will be more in sync with your other visuals.\n\nSome desktop applications may use both kinds of cursors (e.g. enable software cursor only when resizing/dragging something).");
            ImGui::Text("Also see Style->Rendering for rendering options.");
            ImGui::TreePop();
            ImGui::Separator();
        }

        if (ImGui::TreeNode("Backend Flags"))
        {
            HelpMarker(
                "Those flags are set by the back-ends (imgui_impl_xxx files) to specify their capabilities.\n"
                "Here we expose then as read-only fields to avoid breaking interactions with your back-end.");

            // Make a local copy to avoid modifying actual back-end flags.
            ImGuiBackendFlags backend_flags = io.BackendFlags;
            ImGui::CheckboxFlags("io.BackendFlags: HasGamepad", (unsigned int *)&backend_flags, ImGuiBackendFlags_HasGamepad);
            ImGui::CheckboxFlags("io.BackendFlags: HasMouseCursors", (unsigned int *)&backend_flags, ImGuiBackendFlags_HasMouseCursors);
            ImGui::CheckboxFlags("io.BackendFlags: HasSetMousePos", (unsigned int *)&backend_flags, ImGuiBackendFlags_HasSetMousePos);
            ImGui::CheckboxFlags("io.BackendFlags: RendererHasVtxOffset", (unsigned int *)&backend_flags, ImGuiBackendFlags_RendererHasVtxOffset);
            ImGui::TreePop();
            ImGui::Separator();
        }

        if (ImGui::TreeNode("Style"))
        {
            HelpMarker("The same contents can be accessed in 'Tools->Style Editor' or by calling the ShowStyleEditor() function.");
            ImGui::ShowStyleEditor();
            ImGui::TreePop();
            ImGui::Separator();
        }

        if (ImGui::TreeNode("Capture/Logging"))
        {
            HelpMarker(
                "The logging API redirects all text output so you can easily capture the content of "
                "a window or a block. Tree nodes can be automatically expanded.\n"
                "Try opening any of the contents below in this window and then click one of the \"Log To\" button.");
            ImGui::LogButtons();

            HelpMarker("You can also call ImGui::LogText() to output directly to the log without a visual output.");
            if (ImGui::Button("Copy \"Hello, world!\" to clipboard"))
            {
                ImGui::LogToClipboard();
                ImGui::LogText("Hello, world!");
                ImGui::LogFinish();
            }
            ImGui::TreePop();
        }
    }

    displayRenderablesMenu();
}



void GUI::displayRenderablesMenu()
{
    ImGui::Begin("Renderables");

    if (ImGui::TreeNode("Camera"))
    {

        Camera & cam = m_viewer.getCamera();
        glm::vec3 camPos = cam.getPosition();
        glm::vec3 camForward = cam.getForward();
        //glm::vec3 camRight = cam.getRight();
        glm::vec3 camUp = cam.getUp();

        ImGui::Text("Pos X : %.5f", camPos.x);
        ImGui::Text("Pos Y : %.5f", camPos.y);
        ImGui::Text("Pos Z : %.5f", camPos.z);

        ImGui::Text("Dir X : %.5f", camForward.x);
        ImGui::Text("Dir Y : %.5f", camForward.y);
        ImGui::Text("Dir Z : %.5f", camForward.z);

        ImGui::Text("Up X : %.5f", camUp.x);
        ImGui::Text("Up Y : %.5f", camUp.y);
        ImGui::Text("Up Z : %.5f", camUp.z);

        ImGui::TreePop();
        ImGui::Separator();
    }

    auto renderables = m_viewer.getRenderables();

    int i=0;
    for(RenderablePtr r : renderables)
    {
        
        
        // If this is not a HierarchicalRenderablePtr, we continue
        HierarchicalRenderablePtr hr = std::dynamic_pointer_cast<HierarchicalRenderable>(r);
        if (hr == nullptr) 
            continue;
        


        std::string name = "Renderable n°" + std::to_string(i++);
        if (ImGui::TreeNode(name.c_str()))
        {
            glm::vec3 scale;
            glm::quat rotation;
            glm::vec3 translation;
            glm::vec3 skew;
            glm::vec4 perspective;
            glm::decompose(hr->getLocalTransform(), scale, rotation, translation, skew, perspective);

            glm::vec3 euler = glm::eulerAngles(rotation);
            


            ImGui::InputFloat("Pos X", &translation.x);
            ImGui::InputFloat("Pos Y", &translation.y);
            ImGui::InputFloat("Pos Z", &translation.z);
            
            ImGui::SliderFloat("Rotation X", &euler.x, -200.0f, 200.0f, "ratio = %.5f");
            ImGui::SliderFloat("Rotation Y", &euler.y, -200.0f, 200.0f, "ratio = %.5f");
            ImGui::SliderFloat("Rotation Z", &euler.z, -200.0f, 200.0f, "ratio = %.5f");

            ImGui::SliderFloat("Scale X", &scale.x, -200.0f, 200.0f, "ratio = %.5f");
            ImGui::SliderFloat("Scale Y", &scale.y, -200.0f, 200.0f, "ratio = %.5f");
            ImGui::SliderFloat("Scale Z", &scale.z, -200.0f, 200.0f, "ratio = %.5f");

            glm::quat QuatAroundX = glm::angleAxis(glm::radians(euler.x), glm::vec3(1.0, 0.0, 0.0));
            glm::quat QuatAroundY = glm::angleAxis(glm::radians(euler.y), glm::vec3(0.0, 1.0, 0.0));
            glm::quat QuatAroundZ = glm::angleAxis(glm::radians(euler.z), glm::vec3(0.0, 0.0, 1.0));
            glm::quat orientation = QuatAroundX * QuatAroundY * QuatAroundZ;

            glm::mat4 localTransformation = glm::translate(glm::mat4(1.0f), translation);
            localTransformation = glm::toMat4(orientation) * localTransformation;
            localTransformation = glm::scale(localTransformation, scale);
            hr->setLocalTransform(localTransformation);

            ImGui::TreePop();
            ImGui::Separator();
        }
    }
    

    ImGui::End();
}