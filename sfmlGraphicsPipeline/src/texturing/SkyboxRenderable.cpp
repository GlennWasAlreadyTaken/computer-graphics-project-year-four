#include "./../../include/texturing/SkyboxRenderable.hpp"
#include "./../../include/gl_helper.hpp"
#include "./../../include/Utils.hpp"
#include "./../../include/Image.hpp"

#include <glm/gtc/type_ptr.hpp>
#include <GL/glew.h>
#include <iostream>

SkyboxRenderable::~SkyboxRenderable()
{
    glcheck(glDeleteBuffers(1, &m_pBuffer));
}

SkyboxRenderable::SkyboxRenderable(ShaderProgramPtr shaderProgram, const std::vector<std::string>& faces)
    : HierarchicalRenderable(shaderProgram),
      m_pBuffer(0)
{
    //Initialize geometry
    getUnitCube(m_positions);
    loadCubemap(faces);

    //Create buffers
    glcheck(glGenBuffers(1, &m_pBuffer)); //vertices

    //Activate buffer and send data to the graphics card
    glcheck(glBindBuffer(GL_ARRAY_BUFFER, m_pBuffer));
    glcheck(glBufferData(GL_ARRAY_BUFFER, m_positions.size()*sizeof(glm::vec3), m_positions.data(), GL_STATIC_DRAW));
}

void SkyboxRenderable::bindCubemap(ShaderProgramPtr& shader) {
    int skyboxLoc = shader->getUniformLocation("skybox");

    if (skyboxLoc != ShaderProgram::null_location)
    {
        glcheck(glActiveTexture(GL_TEXTURE0));
        glcheck(glBindTexture(GL_TEXTURE_CUBE_MAP, textureID));
        //Send "texSampler" to Texture Unit 0
        glcheck(glUniform1i(skyboxLoc, 0));
    }
}

void SkyboxRenderable::releaseCubemap() {
    //Release texture
    glcheck(glBindTexture(GL_TEXTURE_CUBE_MAP, 0));
}

void SkyboxRenderable::do_draw()
{
    //Location
    int positionLocation = m_shaderProgram->getAttributeLocation("vPosition");
    int nitLocation = m_shaderProgram->getUniformLocation("NIT");

    if( nitLocation != ShaderProgram::null_location )
    {
        glcheck(glUniformMatrix3fv( nitLocation, 1, GL_FALSE,
                                    glm::value_ptr(glm::transpose(glm::inverse(glm::mat3(getModelMatrix()))))));
    }

    if(positionLocation != ShaderProgram::null_location)
    {
        //Activate location
        glcheck(glEnableVertexAttribArray(positionLocation));
        //Bind buffer
        glcheck(glBindBuffer(GL_ARRAY_BUFFER, m_pBuffer));
        //Specify internal format
        glcheck(glVertexAttribPointer(positionLocation, 3, GL_FLOAT, GL_FALSE, 0, (void*)0));
    }

    bindCubemap(m_shaderProgram);

    glcheck(glDepthFunc(GL_LEQUAL)); // change depth function so depth test passes when values are equal to depth buffer's content
    glcheck(glDisable(GL_CULL_FACE));

    //Draw triangles elements
    glcheck(glDrawArrays(GL_TRIANGLES, 0, m_positions.size()));

    //glcheck(glEnable(GL_CULL_FACE));

    glcheck(glDepthFunc(GL_LESS)); // set depth function back to default

    //Release texture
    releaseCubemap();

    if (positionLocation != ShaderProgram::null_location)
    {
        glcheck(glDisableVertexAttribArray(positionLocation));
    }
}

void SkyboxRenderable::do_animate(float time)
{

}

void SkyboxRenderable::loadCubemap(std::vector<std::string> faces)
{
    glcheck(glGenTextures(1, &textureID));
    glcheck(glBindTexture(GL_TEXTURE_CUBE_MAP, textureID));

    
    for (unsigned int i = 0; i < faces.size(); i++)
    {

        Image img;
        if (img.load(faces[i], false))
        {
            int nrChannels = img.nrChannels();
            // Looking for the format
            assert((1 <= nrChannels) && (4 >= nrChannels));
            GLenum glformat;
            switch (nrChannels)
            {
            case 1:
                glformat = GL_RED;
                break;
            case 2:
                glformat = GL_RG;
                break;
            case 3:
                glformat = GL_RGB;
                break;
            case 4:
                glformat = GL_RGBA;
                break;
            }

            glcheck(glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i,
                         0, GL_RGB, img.width(), img.height(), 0, glformat, GL_UNSIGNED_BYTE, img.data()));
        }
        else
        {
            std::cout << "Cubemap tex failed to load at path: " << faces[i] << std::endl;
        }
    }
    glcheck(glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR));
    glcheck(glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR));
    glcheck(glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE));
    glcheck(glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE));
    glcheck(glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE));
}
