#include "./../../include/texturing/TexturedLightedMeshRenderable.hpp"
#include "./../../include/gl_helper.hpp"
#include "./../../include/log.hpp"
#include "./../../include/Io.hpp"
#include "./../../include/Utils.hpp"

#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <GL/glew.h>

TexturedLightedMeshRenderable::~TexturedLightedMeshRenderable()
{
    glcheck(glDeleteBuffers(1, &m_pBuffer));
    glcheck(glDeleteBuffers(1, &m_cBuffer));
    glcheck(glDeleteBuffers(1, &m_nBuffer));
    glcheck(glDeleteBuffers(1, &m_tBuffer));

    for (int i = 0; i < m_iBuffers.size(); ++i)
    {
        glcheck(glDeleteBuffers(1, &(m_iBuffers[i])));
    }
        
}

TexturedLightedMeshRenderable::TexturedLightedMeshRenderable(
    ShaderProgramPtr shaderProgram, const std::string &mesh_filename) : HierarchicalRenderable(shaderProgram),
m_pBuffer(0), m_cBuffer(0), m_nBuffer(0), m_tBuffer(0)
{
    read_obj(mesh_filename, m_positions, m_normals, m_texCoords, m_materials, m_indicesPerMaterial);    

    //Create buffers
    glcheck(glGenBuffers(1, &m_pBuffer)); //vertices
    glcheck(glGenBuffers(1, &m_cBuffer)); //colors
    glcheck(glGenBuffers(1, &m_nBuffer)); //normals
    glcheck(glGenBuffers(1, &m_tBuffer)); //texture coordinates

    m_iBuffers.resize(m_indicesPerMaterial.size());

    for (int i = 0; i < m_indicesPerMaterial.size(); ++i)
    {
        glcheck(glGenBuffers(1, &(m_iBuffers[i]))); //indices
        glcheck(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_iBuffers[i]));
        glcheck(glBufferData(GL_ELEMENT_ARRAY_BUFFER, m_indicesPerMaterial[i].size() * sizeof(unsigned int), m_indicesPerMaterial[i].data(), GL_STATIC_DRAW));
    }

    /*
    std::cout << "/////////////////////////////////////////////////////////////////////////////////////////////////" << std::endl;
    std::cout << "/////////////////////////////////////////////////////////////////////////////////////////////////" << std::endl;
    std::cout << "/////////////////////////////////////////////////////////////////////////////////////////////////" << std::endl;
    std::cout << "*************************** mesh_filename:  " << mesh_filename << " *****************************" << std::endl;
    std::cout << "*************************** position size:  " << m_positions.size() << std::endl;
    std::cout << "*************************** m_texCoords size:  " << m_texCoords.size() << std::endl;
    std::cout << "*************************** m_normals size:  " << m_normals.size() << std::endl;
    std::cout << "*************************** m_iBuffers size:  " << m_iBuffers.size() << std::endl;
    

    for (int i = 0; i < m_indicesPerMaterial.size(); ++i)
    {

        std::cout << "--------------------------- how many indices? :  " << m_indicesPerMaterial[i].size() << std::endl;
        std::cout << "--------------------------- material :  " << std::endl;
        std::cout << "ambient: " << m_materials[i]->ambient().x << ";" << m_materials[i]->ambient().y << ";" << m_materials[i]->ambient().z << std::endl;
        std::cout << "diffuse: " << m_materials[i]->diffuse().x << ";" << m_materials[i]->diffuse().y << ";" << m_materials[i]->diffuse().z << std::endl;
        std::cout << "specular: " << m_materials[i]->specular().x << ";" << m_materials[i]->specular().y << ";" << m_materials[i]->specular().z << std::endl;
    }
    */
    

    //Activate buffer and send data to the graphics card
    glcheck(glBindBuffer(GL_ARRAY_BUFFER, m_pBuffer));
    glcheck(glBufferData(GL_ARRAY_BUFFER, m_positions.size()*sizeof(glm::vec3), m_positions.data(), GL_STATIC_DRAW));
    glcheck(glBindBuffer(GL_ARRAY_BUFFER, m_cBuffer));
    glcheck(glBufferData(GL_ARRAY_BUFFER, m_colors.size()*sizeof(glm::vec4), m_colors.data(), GL_STATIC_DRAW));
    glcheck(glBindBuffer(GL_ARRAY_BUFFER, m_nBuffer));
    glcheck(glBufferData(GL_ARRAY_BUFFER, m_normals.size()*sizeof(glm::vec3), m_normals.data(), GL_STATIC_DRAW));
    glcheck(glBindBuffer(GL_ARRAY_BUFFER, m_tBuffer));
    glcheck(glBufferData(GL_ARRAY_BUFFER, m_texCoords.size()*sizeof(glm::vec2), m_texCoords.data(), GL_STATIC_DRAW));
    
}

void TexturedLightedMeshRenderable::do_draw()
{
    //Location
    int positionLocation = m_shaderProgram->getAttributeLocation("vPosition");
    int colorLocation = m_shaderProgram->getAttributeLocation("vColor");
    int normalLocation = m_shaderProgram->getAttributeLocation("vNormal");
    int texcoordLocation = m_shaderProgram->getAttributeLocation("vTexCoord");
    int modelLocation = m_shaderProgram->getUniformLocation("modelMat");
    int nitLocation = m_shaderProgram->getUniformLocation("NIT");
    
    //Send data to GPU
    if(modelLocation != ShaderProgram::null_location)
    {
        glcheck(glUniformMatrix4fv(modelLocation, 1, GL_FALSE, glm::value_ptr(getModelMatrix())));
    }

    if(positionLocation != ShaderProgram::null_location)
    {
        //Activate location
        glcheck(glEnableVertexAttribArray(positionLocation));
        //Bind buffer
        glcheck(glBindBuffer(GL_ARRAY_BUFFER, m_pBuffer));
        //Specify internal format
        glcheck(glVertexAttribPointer(positionLocation, 3, GL_FLOAT, GL_FALSE, 0, (void*)0));
    }

    if(colorLocation != ShaderProgram::null_location)
    {
        glcheck(glEnableVertexAttribArray(colorLocation));
        glcheck(glBindBuffer(GL_ARRAY_BUFFER, m_cBuffer));
        glcheck(glVertexAttribPointer(colorLocation, 4, GL_FLOAT, GL_FALSE, 0, (void*)0));
    }

    if(normalLocation != ShaderProgram::null_location)
    {
        glcheck(glEnableVertexAttribArray(normalLocation));
        glcheck(glBindBuffer(GL_ARRAY_BUFFER, m_nBuffer));
        glcheck(glVertexAttribPointer(normalLocation, 3, GL_FLOAT, GL_FALSE, 0, (void*)0));
    }

    if( nitLocation != ShaderProgram::null_location )
      {
        glcheck(glUniformMatrix3fv( nitLocation, 1, GL_FALSE,
          glm::value_ptr(glm::transpose(glm::inverse(glm::mat3(getModelMatrix()))))));
      }

    //Bind texture in Textured Unit 0
    if(texcoordLocation != ShaderProgram::null_location)
    {
        glcheck(glEnableVertexAttribArray(texcoordLocation));
        glcheck(glBindBuffer(GL_ARRAY_BUFFER, m_tBuffer));
        glcheck(glVertexAttribPointer(texcoordLocation, 2, GL_FLOAT, GL_FALSE, 0, (void*)0));
    }

    for(int i=0; i < m_materials.size(); i++)
    {
        //Send material uniform to GPU
        Material::sendToGPU(m_shaderProgram, m_materials[i]);
        
        //Draw triangles elements
        glcheck(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_iBuffers[i]));
        glcheck(glDrawElements(GL_TRIANGLES, m_indicesPerMaterial[i].size(), GL_UNSIGNED_INT, (void *)0));

        m_materials[i]->textureRelease();
    }

    if(positionLocation != ShaderProgram::null_location)
    {
        glcheck(glDisableVertexAttribArray(positionLocation));
    }

    if(colorLocation != ShaderProgram::null_location)
    {
        glcheck(glDisableVertexAttribArray(colorLocation));
    }

    if(normalLocation != ShaderProgram::null_location)
    {
        glcheck(glDisableVertexAttribArray(normalLocation));
    }
    if(texcoordLocation != ShaderProgram::null_location)
    {
        glcheck(glDisableVertexAttribArray(texcoordLocation));
    }
}

void TexturedLightedMeshRenderable::do_animate(float time) {}

void TexturedLightedMeshRenderable::do_keyPressedEvent(sf::Event &e)
{
    //std::cout << "coucou je menvol" << std::endl;
    //setLocalTransform(glm::scale(getLocalTransform(), glm::vec3(1.001, 1.001, 1.001)));
}

/*
void TexturedLightedMeshRenderable::setMaterial(const MaterialPtr& material)
{
    m_material = material;
}
*/
