#include "./../include/Io.hpp"
#include <glm/glm.hpp>
#include <iostream>
#include <string>
#define TINYOBJLOADER_IMPLEMENTATION // define this in only *one* .cc
#include "tiny_obj_loader.h"

std::string getPathName(const std::string &s)
{
    char sep = '/';

    size_t i = s.rfind(sep, s.length());
    if (i != std::string::npos)
    {
        return (s.substr(0, i) + "/");
    }

    return ("");
}

unsigned int getIndex(const unsigned int &localIndex, const unsigned int &texCoords, const unsigned int &normal, const vToVtVn& map, unsigned int& globalIndex)
{
    unsigned int index = 0;

    std::pair<unsigned int, unsigned int> pair;
    pair.first = texCoords;
    pair.second = normal;

    auto m = map[localIndex];
    auto search = m.find(pair);

    if (search != m.end())
    {
        // Found, so
        index = search->second;
    }
    else
    {
        // Not found : create the index and insert the pair
        // We take the (size of m) + 1 to reference this line
        index = globalIndex++;

        m.insert(std::pair<std::pair<unsigned int, unsigned int>, unsigned int>(pair, index));        
    }

    return index;
}

bool read_obj(const std::string &filename,
              const std::string &path,
              std::vector<glm::vec3> &positions,
              std::vector<glm::vec3> &normals,
              std::vector<glm::vec2> &texcoords,
              std::vector<MaterialPtr> &materials,
              std::vector<std::vector<unsigned int>> &indicesPerMaterial)
{
    tinyobj::attrib_t attrib;
    std::vector<tinyobj::shape_t> shapes;
    std::vector<tinyobj::material_t> materials_to;

    std::string warn;
    std::string err;

    bool ret = tinyobj::LoadObj(&attrib, &shapes, &materials_to, &warn, &err, filename.c_str(), path.c_str());

    if (!warn.empty())
    {
        std::cout << "WARN: " << warn << std::endl;
    }
    if (!err.empty())
    {
        std::cerr << err << std::endl;
    }

    if (!ret) 
    {
        return ret;
    }

    positions.clear();
    normals.clear();
    texcoords.clear();
    materials.clear();
    indicesPerMaterial.clear();
    indicesPerMaterial.resize(materials_to.size() == 0 ? 1 : materials_to.size());

    /*
    std::cout << "Shapes size : " << shapes.size() << std::endl;
    std::cout << " ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::" << std::endl;
    std::cout << " ::::::::::::::::::::::::::::::::::::::  filename : " << filename << std::endl;
    std::cout << " ::::::::::::::::::::::::::::::::::::::  positions : " << attrib.vertices.size() / 3 << std::endl;
    std::cout << " ::::::::::::::::::::::::::::::::::::::  normals : " << attrib.normals.size() / 3 << std::endl;
    std::cout << " ::::::::::::::::::::::::::::::::::::::  texcoords : " << attrib.texcoords.size() / 2 << std::endl;
    */
/*
    assert((attrib.vertices.size() % 3) == 0);
    for (size_t v = 0; v < attrib.vertices.size() / 3; v++)
    {
        positions.push_back(glm::vec3(attrib.vertices[3 * v + 0], attrib.vertices[3 * v + 1], attrib.vertices[3 * v + 2]));
    }
*/
    positions.resize(attrib.vertices.size()/3);
    normals.resize(attrib.normals.size()/3);
    
    texcoords.resize(attrib.texcoords.size()/2);
    for(int i = 0; i < texcoords.size(); ++i) {
        texcoords[i] = glm::vec2(-1, -1);
    }

    vToVtVn map;
    map.resize(attrib.vertices.size() / 3);
    unsigned int globalIndex = 0;

    for (size_t s = 0; s < shapes.size(); s++)
    {
        // For each triangle
        for (size_t f = 0; f < shapes[s].mesh.indices.size() / 3; f++)
        {
            tinyobj::index_t idx0 = shapes[s].mesh.indices[3 * f + 0];
            tinyobj::index_t idx1 = shapes[s].mesh.indices[3 * f + 1];
            tinyobj::index_t idx2 = shapes[s].mesh.indices[3 * f + 2];

            assert(idx0.vertex_index >= 0);
            assert(idx1.vertex_index >= 0);
            assert(idx2.vertex_index >= 0);

            int current_material_id = shapes[s].mesh.material_ids[f];

            // If there's no material, we reference each vertex in a default material
            if (current_material_id < 0) current_material_id = 0;


            unsigned int idx0Global = getIndex(idx0.vertex_index, idx0.texcoord_index, idx0.normal_index, map, globalIndex);
            unsigned int idx1Global = getIndex(idx1.vertex_index, idx1.texcoord_index, idx1.normal_index, map, globalIndex);
            unsigned int idx2Global = getIndex(idx2.vertex_index, idx2.texcoord_index, idx2.normal_index, map, globalIndex);

            // Filling the indices per material
            indicesPerMaterial[current_material_id].push_back(idx0Global);
            indicesPerMaterial[current_material_id].push_back(idx1Global);
            indicesPerMaterial[current_material_id].push_back(idx2Global);

            positions.insert(positions.begin() + idx0Global, glm::vec3(attrib.vertices[3 * idx0.vertex_index], attrib.vertices[3 * idx0.vertex_index + 1], attrib.vertices[3 * idx0.vertex_index + 2]));
            positions.insert(positions.begin() + idx1Global, glm::vec3(attrib.vertices[3 * idx1.vertex_index], attrib.vertices[3 * idx1.vertex_index + 1], attrib.vertices[3 * idx1.vertex_index + 2]));
            positions.insert(positions.begin() + idx2Global, glm::vec3(attrib.vertices[3 * idx2.vertex_index], attrib.vertices[3 * idx2.vertex_index + 1], attrib.vertices[3 * idx2.vertex_index + 2]));

            normals.insert(normals.begin() + idx0Global, glm::vec3(attrib.normals[3 * idx0.normal_index], attrib.normals[3 * idx0.normal_index + 1], attrib.normals[3 * idx0.normal_index + 2]));
            normals.insert(normals.begin() + idx1Global, glm::vec3(attrib.normals[3 * idx1.normal_index], attrib.normals[3 * idx1.normal_index + 1], attrib.normals[3 * idx1.normal_index + 2]));
            normals.insert(normals.begin() + idx2Global, glm::vec3(attrib.normals[3 * idx2.normal_index], attrib.normals[3 * idx2.normal_index + 1], attrib.normals[3 * idx2.normal_index + 2]));

            texcoords.insert(texcoords.begin() + idx0Global, glm::vec2(attrib.texcoords[2 * idx0.texcoord_index], attrib.texcoords[2 * idx0.texcoord_index + 1]));
            texcoords.insert(texcoords.begin() + idx1Global, glm::vec2(attrib.texcoords[2 * idx1.texcoord_index], attrib.texcoords[2 * idx1.texcoord_index + 1]));
            texcoords.insert(texcoords.begin() + idx2Global, glm::vec2(attrib.texcoords[2 * idx2.texcoord_index], attrib.texcoords[2 * idx2.texcoord_index + 1]));
        }
    }
    
    /*
    std::cout << "--------------------------" << std::endl;

    std::cout << "materials_to size : " << materials_to.size() << std::endl;
    std::cout << "indicesPerMaterial size : " << indicesPerMaterial.size() << std::endl;
*/
    for (size_t i = 0; i < materials_to.size(); i++)
    {
        // Creating a new material
        MaterialPtr material = std::make_shared<Material>();

        // Loading the material components
        material->setAmbient(glm::vec3(materials_to[i].ambient[0], materials_to[i].ambient[1], materials_to[i].ambient[2]));
        material->setDiffuse(glm::vec3(materials_to[i].diffuse[0], materials_to[i].diffuse[1], materials_to[i].diffuse[2]));
        material->setSpecular(glm::vec3(materials_to[i].specular[0], materials_to[i].specular[1], materials_to[i].specular[2]));
        material->setEmission(glm::vec3(materials_to[i].emission[0], materials_to[i].emission[1], materials_to[i].emission[2]));
        material->setShininess(materials_to[i].shininess);
        material->setTextured(false);

        // If the ambient and diffuse are the same, we load the texture only once
        if (materials_to[i].ambient_texname == materials_to[i].diffuse_texname && materials_to[i].ambient_texname != "")
        {
            TexturePtr tex = std::make_shared<Texture>(path + materials_to[i].ambient_texname);
            material->setAmbientTexture(tex);
            material->setDiffuseTexture(tex);

            material->setTextured(true);

            bool emission = false;
            if (materials_to[i].emissive_texname != "")
            {
                TexturePtr e_tex = std::make_shared<Texture>(path + materials_to[i].emissive_texname);
                material->setEmissionTexture(e_tex);
                emission = true;
                material->setEmissive(true);
            }
        }
        else
        {
            bool ambient = false;
            if (materials_to[i].ambient_texname != "")
            {
                TexturePtr a_tex = std::make_shared<Texture>(path + materials_to[i].ambient_texname);
                material->setAmbientTexture(a_tex);
                ambient = true;

                material->setTextured(true);
            }

            bool diffuse = false;
            if (materials_to[i].diffuse_texname != "")
            {
                TexturePtr d_tex = std::make_shared<Texture>(path + materials_to[i].diffuse_texname);
                material->setDiffuseTexture(d_tex);
                diffuse = true;

                if (!ambient)
                    material->setAmbientTexture(d_tex);

                material->setTextured(true);
            }

            bool emission = false;
            if(materials_to[i].emissive_texname != "" && diffuse)
            {
                TexturePtr e_tex = std::make_shared<Texture>(path + materials_to[i].emissive_texname);
                material->setEmissionTexture(e_tex);
                material->setEmissive(true);
                emission = true;
            }

            if(!diffuse && !ambient)
            {
                TexturePtr tex = std::make_shared<Texture>();
                material->setAmbientTexture(tex);
                material->setDiffuseTexture(tex);

                material->setTextured(false);
            }
        }

        materials.insert(materials.begin() + i, material);
    }

    // in case there is not any material in the obj
    if (materials_to.size() == 0)
    {
        MaterialPtr material = std::make_shared<Material>();
        TexturePtr tex = std::make_shared<Texture>();
        material->setAmbientTexture(tex);
        material->setDiffuseTexture(tex);

        material->setTextured(true);
        materials.insert(materials.begin(), material);
    }

    return ret;
}

bool read_obj(const std::string &filename,
              std::vector<glm::vec3> &positions,
              std::vector<unsigned int> &triangles,
              std::vector<glm::vec3> &normals,
              std::vector<glm::vec2> &texcoords)
{
    std::vector<MaterialPtr> materials;
    std::vector<std::vector<unsigned int>> indicesPerMaterial;
    // WARNING: remove the usage of the triangles vector : can create problems in certain classes
    return read_obj(filename, getPathName(filename), positions, normals, texcoords, materials, indicesPerMaterial);
}

bool read_obj(const std::string &filename,
              std::vector<glm::vec3> &positions,
              std::vector<glm::vec3> &normals,
              std::vector<glm::vec2> &texcoords,
              std::vector<MaterialPtr> &materials,
              std::vector<std::vector<unsigned int>> &indicesPerMaterial)
{
    return read_obj(filename, getPathName(filename), positions, normals, texcoords, materials, indicesPerMaterial);
}