# include "../include/KeyframedMeshRenderable.hpp"
# include "../include/gl_helper.hpp"
# include "../include/GeometricTransformation.hpp"
# include "../include/Utils.hpp"
# include "../include/Io.hpp"

# include <glm/gtc/type_ptr.hpp>
# include <GL/glew.h>

KeyframedMeshRenderable::KeyframedMeshRenderable(ShaderProgramPtr prog,
												 const std::string &mesh_filename) : TexturedLightedMeshRenderable(prog, mesh_filename) {}

void KeyframedMeshRenderable::addLocalTransformKeyframe(const GeometricTransformation& transformation, float time)
{
	m_localKeyframes.add(transformation, time);
}

void KeyframedMeshRenderable::addParentTransformKeyframe(const GeometricTransformation& transformation, float time)
{
	m_parentKeyframes.add(transformation, time);
}

void KeyframedMeshRenderable::do_animate(float time)
{
	//Assign the interpolated transformations from the keyframes to the local/parent transformations.
	if (!m_localKeyframes.empty())
	{
		setLocalTransform(m_localKeyframes.interpolateTransformation(time));
	}
	if (!m_parentKeyframes.empty())
	{
		setParentTransform(m_parentKeyframes.interpolateTransformation(time));
	}
}

KeyframedMeshRenderable::~KeyframedMeshRenderable()
{
	TexturedLightedMeshRenderable::~TexturedLightedMeshRenderable();
}