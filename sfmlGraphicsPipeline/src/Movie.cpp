#include "./../include/Movie.hpp"
#include <lighting/DirectionalLightRenderable.hpp>
#include <lighting/PointLightRenderable.hpp>

Movie::Movie(Viewer &viewer) : m_viewer(viewer) 
{
    // VERY IMPORTANT: indicate number of scenes !!!!
    m_viewer.setNumberOfScenes(8);

    // Setting the camera in spaceship mode
    Camera &cam = m_viewer.getCamera();
    cam.setMouseBehavior(Camera::CAMERA_MOUSE_BEHAVIOR::SPACESHIP_BEHAVIOR);

    loadShaders();
    loadRenderables();
    loadSkyboxes();
    loadScenes();
}

void Movie::loadShaders()
{
    //Add shader
    m_skyboxShader = std::make_shared<ShaderProgram>("../../sfmlGraphicsPipeline/shaders/skyboxVertex.glsl",
                                                                    "../../sfmlGraphicsPipeline/shaders/skyboxFragment.glsl");

    m_textureShader = std::make_shared<ShaderProgram>("../../sfmlGraphicsPipeline/shaders/textureVertex.glsl",
                                                                     "../../sfmlGraphicsPipeline/shaders/textureFragment.glsl");

    m_textureReflectSkyboxShader = std::make_shared<ShaderProgram>("../../sfmlGraphicsPipeline/shaders/textureVertex.glsl",
                                                                                  "../../sfmlGraphicsPipeline/shaders/textureFragmentReflectSkybox.glsl");

    m_flatShader = std::make_shared<ShaderProgram>("../../sfmlGraphicsPipeline/shaders/flatVertex.glsl",
                                                   "../../sfmlGraphicsPipeline/shaders/flatFragment.glsl");

    m_viewer.addShaderProgram(m_skyboxShader);
    m_viewer.addShaderProgram(m_textureShader);
    m_viewer.addShaderProgram(m_textureReflectSkyboxShader);
    m_viewer.addShaderProgram(m_flatShader);
}

void Movie::loadRenderables()
{
    m_spaceship = std::make_shared<KeyframedMeshRenderable>(m_textureReflectSkyboxShader, "../../sfmlGraphicsPipeline/meshes/Mothership/vaisseau_mere.obj");
    m_ufo = std::make_shared<KeyframedMeshRenderable>(m_textureReflectSkyboxShader, "../../sfmlGraphicsPipeline/meshes/UFO/UFO_COLORED.obj");
    m_earth = std::make_shared<KeyframedMeshRenderable>(m_textureShader, "../../sfmlGraphicsPipeline/meshes/earth_mesh/Model/Globe.obj");
    m_corridor = std::make_shared<KeyframedMeshRenderable>(m_textureShader, "../../sfmlGraphicsPipeline/meshes/Corridor/corridor_half.obj");
    m_hill = std::make_shared<KeyframedMeshRenderable>(m_textureShader, "../../sfmlGraphicsPipeline/meshes/Hill/Hill.obj");
    m_cow = std::make_shared<KeyframedMeshRenderable>(m_textureShader, "../../sfmlGraphicsPipeline/meshes/cow/cow.obj");
    m_alienLeg = std::make_shared<KeyframedMeshRenderable>(m_textureShader, "../../sfmlGraphicsPipeline/meshes/AlienLeg/AlienLeg.obj");
	m_surrounding = std::make_shared<KeyframedMeshRenderable>(m_textureShader, "../../sfmlGraphicsPipeline/meshes/Stage/Wall.obj");
	m_stage = std::make_shared<KeyframedMeshRenderable>(m_textureShader, "../../sfmlGraphicsPipeline/meshes/WoodenFloor/ChallengeStage/challenge_stage.obj");
}

void Movie::loadSkyboxes()
{
    loadSpaceSkybox();
    loadEarthSkybox();
}

void Movie::loadScenes()
{
	outOfLightSpeed(0);
	runningAliens(1);
	docking(2);
	ufoOutOfMotherShip(3);
	ufoToEarth(4);
	cowAbducted(5);
	ufoToSpace(6);
	cowStage(7);
    
    m_viewer.setAnimationLoop(true, 0.0f);
}

void Movie::start()
{
    m_viewer.startAnimation();
}

// ---------------------------------------------------------------------------------------------------------------------------------------
// Scenes
// ---------------------------------------------------------------------------------------------------------------------------------------
void Movie::outOfLightSpeed(int sceneNumber)
{
    setSpaceSkybox(sceneNumber);

    glm::vec3 camPos{-3.26, 14.76, -15.87};
    glm::vec3 camForward{0.667, -0.449, 0.593};
    glm::vec3 camUp{0.249, 0.886, 0.389};

    m_viewer.setViewMatrix(sceneNumber, glm::lookAt(
                                            camPos,
                                            camPos + camForward,
                                            camUp));

	KeyframedMeshRenderablePtr spaceship_cpy = std::make_shared<KeyframedMeshRenderable>(*m_spaceship);
    m_viewer.addRenderable(sceneNumber, spaceship_cpy);
	
    //========= Animation =========//
    float endTimestamp = 7.0f;

    GeometricTransformation gt1 = getGT(glm::vec3{0, 0, 0}, glm::vec3{0, 0, 0}, glm::vec3{0, 0, 0});
    GeometricTransformation gt2 = getGT(glm::vec3{20, 0, 0}, glm::vec3{0, 0, 0}, glm::vec3{5, 0.4, 0.4});
    GeometricTransformation gt3 = getGT(glm::vec3{21, 0, 0}, glm::vec3{0, 0, 0}, glm::vec3{5, 5, 5});
    GeometricTransformation gt4 = getGT(glm::vec3{35, 0, 0}, glm::vec3{0, 0, 0}, glm::vec3{5, 5, 5});
	spaceship_cpy->addLocalTransformKeyframe(gt1, 0.0f);
	spaceship_cpy->addLocalTransformKeyframe(gt1, 3.0f);
	spaceship_cpy->addLocalTransformKeyframe(gt2, 3.1f);
	spaceship_cpy->addLocalTransformKeyframe(gt3, 3.2f);
	spaceship_cpy->addLocalTransformKeyframe(gt4, endTimestamp);

    // VERY IMPORTANT
    m_viewer.setAnimationDuration(sceneNumber, endTimestamp);
}

void Movie::runningAliens(int sceneNumber)
{
    glm::mat4 localTransformation;
    glm::vec3 lightPosition(0, 1, 0);
    glm::vec3 d_direction = glm::normalize(glm::vec3(0, 1, 0));
    glm::vec3 d_ambient(0.3, 0.3, 0.3), d_diffuse(0.75, 0.75, 0.75), d_specular(0.6, 0.6, 0.6);
    DirectionalLightPtr directionalLight = std::make_shared<DirectionalLight>(d_direction, d_ambient, d_diffuse, d_specular);
    m_viewer.setDirectionalLight(sceneNumber, directionalLight);

    float p_constant = 1.0, p_linear = 5e-1, p_quadratic = 0.0;

    for (int i = 0; i < 32; i += 8)
    {
        m_viewer.addPointLight(sceneNumber, std::make_shared<PointLight>(glm::vec3(i, 2, 0), d_ambient, d_diffuse, d_specular, p_constant, p_linear, p_quadratic));
        //m_viewer.addPointLight(sceneNumber, std::make_shared<PointLight>(glm::vec3(i, 2, -2), d_ambient, d_diffuse, d_specular, p_constant, p_linear, p_quadratic));
        //m_viewer.addPointLight(sceneNumber, std::make_shared<PointLight>(glm::vec3(i, 2, 2), d_ambient, d_diffuse, d_specular, p_constant, p_linear, p_quadratic));
    }
	KeyframedMeshRenderablePtr leg1 = std::make_shared<KeyframedMeshRenderable>(*m_alienLeg);
    KeyframedMeshRenderablePtr leg2 = std::make_shared<KeyframedMeshRenderable>(*m_alienLeg);
    KeyframedMeshRenderablePtr leg3 = std::make_shared<KeyframedMeshRenderable>(*m_alienLeg);
    KeyframedMeshRenderablePtr leg4 = std::make_shared<KeyframedMeshRenderable>(*m_alienLeg);

    KeyframedMeshRenderablePtr legs1[] = {leg1, leg2, leg3, leg4};
    int i = 0;
    for (i = 0; i < 4; i++)
    {
        m_viewer.addRenderable(sceneNumber, legs1[i]);
        legs1[i]->setLocalTransform(GeometricTransformation(glm::vec3{ i - 3.25f, 0, 0 }, glm::quat(glm::vec3{ 0, M_PI / 2, -M_PI / 4 }), glm::vec3{ 1, 1, 1 }).toMatrix());
    }
    runningAliensAnimation(legs1, 0.0f);

    KeyframedMeshRenderablePtr leg5 = std::make_shared<KeyframedMeshRenderable>(*m_alienLeg);
    KeyframedMeshRenderablePtr leg6 = std::make_shared<KeyframedMeshRenderable>(*m_alienLeg);
    KeyframedMeshRenderablePtr leg7 = std::make_shared<KeyframedMeshRenderable>(*m_alienLeg);
    KeyframedMeshRenderablePtr leg8 = std::make_shared<KeyframedMeshRenderable>(*m_alienLeg);

    KeyframedMeshRenderablePtr legs2[] = {leg5, leg6, leg7, leg8};
    for (i = 0; i < 4; i++)
    {
        m_viewer.addRenderable(sceneNumber, legs2[i]);
        legs2[i]->setLocalTransform(GeometricTransformation(glm::vec3{i - 3 , 0, 0.5f}, glm::quat(glm::vec3{0, M_PI / 2, -M_PI / 4}), glm::vec3{1, 1, 1}).toMatrix());
    }

    float endTimestamp = runningAliensAnimation(legs2, 0.31f);

  
    glm::vec3 camPos{-0.05, 0.57, -1.43};
    glm::vec3 camForward{0.88, -0.46, 0.09};
    glm::vec3 camUp{0, 1, 0};

    m_viewer.setViewMatrix(sceneNumber, glm::lookAt(
                                            camPos,
                                            camPos + camForward,
                                            camUp));
    /*
    m_viewer.getCamera().addTransformKeyframe(CameraTransformation(camPos, camForward, camUp), 7.01f);
    m_viewer.getCamera().addTransformKeyframe(CameraTransformation(camPos, camForward, camUp), endTimestamp + 7.01f);
    */
	KeyframedMeshRenderablePtr corridor = std::make_shared<KeyframedMeshRenderable>(*m_corridor);
    corridor->addLocalTransformKeyframe(GeometricTransformation(glm::vec3{0, -0.5, 0}, glm::quat(glm::vec3{0, 0, 0}), glm::vec3{1, 1, 1}), 0.0f);
    corridor->addLocalTransformKeyframe(GeometricTransformation(glm::vec3{0, -0.5, 0}, glm::quat(glm::vec3{0, 0, 0}), glm::vec3{1, 1, 1}), endTimestamp);
    m_viewer.addRenderable(sceneNumber, corridor);

    // VERY IMPORTANT
    m_viewer.setAnimationDuration(sceneNumber, endTimestamp - 1);
}

float Movie::runningAliensAnimation(KeyframedMeshRenderablePtr* legs, float delay)
{

    float delayLoc = delay;
    int i = 0;
    for (i = 0; i < 4; i++)
    {
        makeStep(legs[i], delayLoc, glm::vec3{0.0f, 0.0f, 0.0f});
        delayLoc += 0.155f;
    }
    for (i = 0; i < 4; i++)
    {
        makeStep(legs[i], delayLoc, glm::vec3{1.0f, 0.0f, 0.0f});
        delayLoc += 0.155f;
    }
    for (i = 0; i < 4; i++)
    {
        makeStep(legs[i], delayLoc, glm::vec3{2.0f, 0.0f, 0.0f});
        delayLoc += 0.155f;
    }

    return delayLoc + 1.0f;
}

void Movie::makeStep(KeyframedMeshRenderablePtr leg, float delay, glm::vec3 offset)
{
    glm::mat4 local = leg->getLocalTransform();
    GeometricTransformation gt1 = getGT(glm::vec3{local[3][0] + 0 + offset.x, local[3][1] + 0 + offset.y, local[3][2] + 0 + offset.z}, glm::vec3{0, M_PI / 2, -M_PI / 4}, glm::vec3{1, 1, 1});
    GeometricTransformation gt2 = getGT(glm::vec3{local[3][0] + 0.5f + offset.x, local[3][1] + 0.6f + offset.y, local[3][2] + 0 + offset.z}, glm::vec3{0, M_PI / 2, -M_PI / 3}, glm::vec3{1, 1, 1});
    GeometricTransformation gt3 = getGT(glm::vec3{local[3][0] + 0.7f + offset.x, local[3][1] + 0.3f + offset.y, local[3][2] + 0 + offset.z}, glm::vec3{0, M_PI / 2, -M_PI / 3.5f}, glm::vec3{1, 1, 1});
    GeometricTransformation gt4 = getGT(glm::vec3{local[3][0] + 1 + offset.x, local[3][1] + 0 + offset.y, local[3][2] + 0 + offset.z}, glm::vec3{0, M_PI / 2, -M_PI / 4}, glm::vec3{1, 1, 1});
    leg->addLocalTransformKeyframe(gt1, delay + 0.0f);
    leg->addLocalTransformKeyframe(gt2, delay + 0.1f);
    leg->addLocalTransformKeyframe(gt3, delay + 0.2f);
    leg->addLocalTransformKeyframe(gt4, delay + 0.3f);
}

void Movie::ufoOutOfMotherShip(int sceneNumber)
{
	setSpaceSkybox(sceneNumber);

	glm::vec3 camPos{ 1.7, 2.0, -3.6 };
	glm::vec3 camForward{ 0.83, -0.24, 0.49 };
	glm::vec3 camUp{ 0.20, 0.97, 0.12 };

	m_viewer.setViewMatrix(sceneNumber, glm::lookAt(
		camPos,
		camPos + camForward,
		camUp));

    // TODO: instead of doing a copy, add a function removeChild() 
    KeyframedMeshRenderablePtr spaceship_cpy = std::make_shared<KeyframedMeshRenderable>(*m_spaceship);
    KeyframedMeshRenderablePtr ufo_cpy = std::make_shared<KeyframedMeshRenderable>(*m_ufo);

    m_viewer.addRenderable(sceneNumber, spaceship_cpy);
    m_viewer.addRenderable(sceneNumber, ufo_cpy);

    spaceship_cpy->setLocalTransform(GeometricTransformation(glm::vec3{0, 0, 0}, glm::quat(glm::vec3{0, 0, 0}), glm::vec3{1, 1, 1}).toMatrix());
    ufo_cpy->setLocalTransform(GeometricTransformation(glm::vec3{0, -0.2, 0}, glm::quat(glm::vec3{0, 0, 0}), glm::vec3{0.01, 0.01, 0.01}).toMatrix());

    KeyframedMeshRenderable::addChild(spaceship_cpy, ufo_cpy);

    //========= Animation =========//
    //Phase 1 : global translation
    GeometricTransformation gt1 = getGT(glm::vec3{0, 0, 0}, glm::vec3{0, 0, 0}, glm::vec3{1, 1, 1});
    GeometricTransformation gt2 = getGT(glm::vec3{5, 0, 0}, glm::vec3{0, 0, 0}, glm::vec3{1, 1, 1});
    spaceship_cpy->addParentTransformKeyframe(gt1, 0.0f);
    spaceship_cpy->addParentTransformKeyframe(gt2, 3.0f);

    //Phase 2_a : local translation of spaceship_cpy
    gt1 = getGT(glm::vec3{0, 0, 0}, glm::vec3{0, 0, 0}, glm::vec3{1, 1, 1});
    gt2 = getGT(glm::vec3{5, 0, 0}, glm::vec3{0, 0, 0}, glm::vec3{1, 1, 1});
    spaceship_cpy->addLocalTransformKeyframe(gt1, 3.0f);
    spaceship_cpy->addLocalTransformKeyframe(gt2, 6.0f);

    //Phase 2_b : local movement of UFO
    gt1 = getGT(glm::vec3{0, -0.2f, 0}, glm::vec3{0, 0, 0}, glm::vec3{0.05, 0.05, 0.05});
    gt2 = getGT(glm::vec3{0, -0.8f, -0.5f}, glm::vec3{0, 0, 0}, glm::vec3{0.05, 0.05, 0.05});
    GeometricTransformation gt3 = getGT(glm::vec3{0, -0.9f, -1.0f}, glm::vec3{0, 0, 0}, glm::vec3{0.05, 0.05, 0.05});
    GeometricTransformation gt4 = getGT(glm::vec3{0, -0.6f, -2.0f}, glm::vec3{0, 0, 0}, glm::vec3{0.05, 0.05, 0.05});
    GeometricTransformation gt5 = getGT(glm::vec3{0, 0.1f, -3.5f}, glm::vec3{0, 0, 0}, glm::vec3{0.05, 0.05, 0.05});
    GeometricTransformation gt6 = getGT(glm::vec3{0, 0.9f, -5}, glm::vec3{0, 0, 0}, glm::vec3{0.05, 0.05, 0.05});
    ufo_cpy->addLocalTransformKeyframe(gt1, 3.0f);
    ufo_cpy->addLocalTransformKeyframe(gt2, 3.5f);
    ufo_cpy->addLocalTransformKeyframe(gt3, 4.0f);
    ufo_cpy->addLocalTransformKeyframe(gt4, 4.5f);
    ufo_cpy->addLocalTransformKeyframe(gt5, 5.0f);
    ufo_cpy->addLocalTransformKeyframe(gt6, 5.5f);


	// VERY IMPORTANT
	m_viewer.setAnimationDuration(sceneNumber, 6.5f);
}

void Movie::cowAbducted(int sceneNumber) {

	setEarthSkybox(sceneNumber);

	glm::vec3 camPos{ -16.93, 15.08, 14.64 };
	glm::vec3 camForward{ -0.086, 0.40, -0.91 };
	glm::vec3 camUp{ -0.04, 0.91, 0.41 };

	m_viewer.setViewMatrix(sceneNumber, glm::lookAt(
		camPos,
		camPos + camForward,
		camUp));

	KeyframedMeshRenderablePtr ufo_cpy = std::make_shared<KeyframedMeshRenderable>(*m_ufo);

	// UFO
	glm::vec3 ufoStartPos = { 15, 60, 10 };
	ufo_cpy->setLocalTransform(GeometricTransformation(ufoStartPos, glm::quat(glm::vec3{ 0, 0, 0 }), glm::vec3{ 3, 3, 3 }).toMatrix());
	m_viewer.addRenderable(sceneNumber, ufo_cpy);

	TexturedLightedMeshRenderablePtr hill_cpy = std::make_shared<TexturedLightedMeshRenderable>(*m_hill);
	hill_cpy->setLocalTransform(GeometricTransformation(glm::vec3{ -10, 20, -10 }, glm::quat(glm::vec3{ 0, 0, 0 }), glm::vec3{ 3, 3, 3 }).toMatrix());
	m_viewer.addRenderable(sceneNumber, hill_cpy);


	glm::vec3 cowStartPos = { -26,13,-4 };
	KeyframedMeshRenderablePtr cow_cpy = std::make_shared<KeyframedMeshRenderable>(*m_cow);
	cow_cpy->setLocalTransform(GeometricTransformation(cowStartPos, glm::quat(glm::vec3{ 0,0,0 }), glm::vec3{ 0.1,0.1,0.1 }).toMatrix());

	m_viewer.addRenderable(sceneNumber, cow_cpy);

	float animScale = 10.0f;
	float abductTime = 4.0f;
	int nbOfKeyframes = 8;
	float currentTime = abductTime / nbOfKeyframes;
	float timeBeforeAbduct = 2.0f;

	//Animate cow
	GeometricTransformation gt1 = getGT(cowStartPos + animScale * glm::vec3{ 0,0,0 }, glm::vec3{ 0,0,0 }, glm::vec3{ 0.1,0.1,0.1 });
	cow_cpy->addLocalTransformKeyframe(gt1, 0.0f);
	cow_cpy->addLocalTransformKeyframe(gt1, timeBeforeAbduct + 0.0f);

	for (int i = 0; i < nbOfKeyframes; i++) {
		GeometricTransformation gt = getGT(cowStartPos + animScale * glm::vec3{ random(-0.05,0.05) * (1 - currentTime / abductTime), animScale * std::max((exp(currentTime) / exp(abductTime / 2)),1.0f) / (10 * abductTime),random(-0.05,0.05) * (1 - currentTime / abductTime) }, glm::vec3{ random(-M_PI / 16, M_PI / 16) * (1 - currentTime / abductTime),random(-M_PI / 16,M_PI / 16) * (1 - currentTime / abductTime),random(-M_PI / 16,M_PI / 16) * (1 - currentTime / abductTime) }, glm::vec3{ 0.1,0.1,0.1 });

		cow_cpy->addLocalTransformKeyframe(gt, timeBeforeAbduct + currentTime);
		currentTime += abductTime / nbOfKeyframes;
	}
	GeometricTransformation gtbegin = getGT(ufoStartPos, glm::vec3{ 0,0,0 }, glm::vec3{ 0.1f,0.1f,0.1f });
	GeometricTransformation gtend = getGT(glm::vec3{ -26,32,-4 }, glm::vec3{ 0,1,0 }, glm::vec3{ 0.1f,0.1f,0.1f });
	//Animate cow when in ufo
	cow_cpy->addLocalTransformKeyframe(gtend, timeBeforeAbduct + abductTime);
	cow_cpy->addLocalTransformKeyframe(gtbegin, timeBeforeAbduct + abductTime + 2.0f);

	//Animate ufo
	gtbegin = getGT(ufoStartPos, glm::vec3{ 0,0,0 }, glm::vec3{ 3,3,3 });
	gtend = getGT(glm::vec3{ -26,34,-4 }, glm::vec3{ 0,M_PI / 2,0 }, glm::vec3{ 3,3,3 });

	ufo_cpy->addLocalTransformKeyframe(gtbegin, 0.0f);
	ufo_cpy->addLocalTransformKeyframe(gtend, timeBeforeAbduct);
	gtend = getGT(glm::vec3{ -26,34,-4 }, glm::vec3{ 0,3 * M_PI / 4,0 }, glm::vec3{ 3,3,3 });
	ufo_cpy->addLocalTransformKeyframe(gtend, timeBeforeAbduct + abductTime);
	gtbegin = getGT(ufoStartPos, glm::vec3{ 0,M_PI,0 }, glm::vec3{ 3,3,3 });
	ufo_cpy->addLocalTransformKeyframe(gtbegin, timeBeforeAbduct + abductTime + 2.0f);


	// VERY IMPORTANT
	m_viewer.setAnimationDuration(sceneNumber, timeBeforeAbduct + abductTime + 2.0f);
}

void Movie::cowStage(int sceneNumber) {

	setSpaceSkybox(sceneNumber);

	TexturedLightedMeshRenderablePtr stage = std::make_shared<TexturedLightedMeshRenderable>(*m_stage);
	stage->setLocalTransform(GeometricTransformation(glm::vec3{ 0, 0, -1 }, glm::quat(glm::vec3{ 0, 0, 0 }), glm::vec3{ 7, 7, 7 }).toMatrix());
	m_viewer.addRenderable(sceneNumber, stage);

	TexturedLightedMeshRenderablePtr stage1Wall = std::make_shared<TexturedLightedMeshRenderable>(*m_surrounding);
	stage1Wall->setLocalTransform(GeometricTransformation(glm::vec3{ 14,14,0 }, glm::quat(glm::vec3{ 0,0,0 }), glm::vec3{ 14,14,14 }).toMatrix());
	TexturedLightedMeshRenderablePtr stage2Wall = std::make_shared<TexturedLightedMeshRenderable>(*m_surrounding);
	stage2Wall->setLocalTransform(GeometricTransformation(glm::vec3{ 0,14,-14 }, glm::quat(glm::vec3{ 0,M_PI / 2,0 }), glm::vec3{ 14,14,14 }).toMatrix());
	TexturedLightedMeshRenderablePtr stage3Wall = std::make_shared<TexturedLightedMeshRenderable>(*m_surrounding);
	stage3Wall->setLocalTransform(GeometricTransformation(glm::vec3{ -14,14,0 }, glm::quat(glm::vec3{ 0,0,0 }), glm::vec3{ 14,14,14 }).toMatrix());

	m_viewer.addRenderable(sceneNumber, stage1Wall);
	m_viewer.addRenderable(sceneNumber, stage2Wall);
	m_viewer.addRenderable(sceneNumber, stage3Wall);

	glm::mat4 localTransformation;
	glm::vec3 lightPosition(0, 1, 0);
	glm::vec3 d_direction = glm::normalize(glm::vec3(0, 1, 0));
	glm::vec3 d_ambient(0.3, 0.3, 0.3), d_diffuse(0.75, 0.75, 0.75), d_specular(0.6, 0.6, 0.6);
	DirectionalLightPtr directionalLight = std::make_shared<DirectionalLight>(d_direction, d_ambient, d_diffuse, d_specular);
	m_viewer.setDirectionalLight(sceneNumber, directionalLight);

	glm::vec3 camPos{ 0, 10, 13 };
	glm::vec3 camForward{ 0 ,-0.25f, -0.75f };
	glm::vec3 camUp{ 0, 0.75f, -0.25f };

	m_viewer.setViewMatrix(sceneNumber, glm::lookAt(
		camPos,
		camPos + camForward,
		camUp));


	glm::vec3 cowStartPos = { 0,25,0 };
	glm::vec3 cowEndPos = { 0,5,0 };

	KeyframedMeshRenderablePtr cow = std::make_shared<KeyframedMeshRenderable>(*m_cow);
	cow->setLocalTransform(GeometricTransformation(cowStartPos, glm::quat(glm::vec3{ 0,0,0 }), glm::vec3{ 0.1,0.1,0.1 }).toMatrix());

	m_viewer.addRenderable(sceneNumber, cow);

	//ANIMATION
	GeometricTransformation gtbegin = getGT(cowStartPos, glm::vec3{ 0,0,0 }, glm::vec3{ 0.1,0.1,0.1 });
	GeometricTransformation gt1 = getGT({ 0,5,2 }, glm::vec3{ 0,0,M_PI / 2 }, glm::vec3{ 0.1,0.1,0.1 });
	GeometricTransformation gt2 = getGT(cowStartPos, glm::vec3{ 0,0,0 }, glm::vec3{ 0.1,0.1,0.1 });
	GeometricTransformation gtend = getGT(cowEndPos, glm::vec3{ 0,0,0 }, glm::vec3{ 0.1,0.1,0.1 });
	cow->addLocalTransformKeyframe(gtbegin, 2.0f);
	cow->addLocalTransformKeyframe(gtend, 3.0f);
	cow->addLocalTransformKeyframe(gt1, 3.5f);
	cow->addLocalTransformKeyframe(gt1, 4.5f);
	cow->addLocalTransformKeyframe(gtend, 5.5f);

	m_viewer.setAnimationDuration(sceneNumber, 9.0f);
}

void Movie::ufoToEarth(int sceneNumber) {

	setSpaceSkybox(sceneNumber);

	glm::vec3 camPos{ 212.17, 64.41, -5.02 };
	glm::vec3 camForward{ -0.73, 0.04, 0.68 };
	glm::vec3 camUp{ -0.02, 0.99, -0.09 };

	m_viewer.setViewMatrix(sceneNumber, glm::lookAt(
		camPos,
		camPos + camForward,
		camUp));

	KeyframedMeshRenderablePtr ufo = std::make_shared<KeyframedMeshRenderable>(*m_ufo);
	ufo->setLocalTransform(GeometricTransformation(glm::vec3{ 164, 73, -48 }, glm::quat(glm::vec3{ 3.14 / 4, 3.14 / 2, 0 }), glm::vec3{ 10, 10, 10 }).toMatrix());
	
	m_viewer.addRenderable(sceneNumber, ufo);

	KeyframedMeshRenderablePtr earth = std::make_shared<KeyframedMeshRenderable>(*m_earth);
	earth->setLocalTransform(GeometricTransformation(glm::vec3{ 150, 0, 200 }, glm::quat(glm::vec3{ 0, 0, 0 }), glm::vec3{ 7, 7, 7 }).toMatrix());
	earth->addLocalTransformKeyframe(getGT(glm::vec3{ 150, 0, 200 }, glm::vec3{ 0, 0, 0 }, glm::vec3{ 7, 7, 7 }), 0.5f);
	earth->addLocalTransformKeyframe(getGT(glm::vec3{ 150, 0, 200 }, glm::vec3{ 0, -3.14 / 32, 0 }, glm::vec3{ 7, 7, 7 }), 3.0f);

	m_viewer.addRenderable(sceneNumber, earth);

	// ANIMATION
	GeometricTransformation gt1 = getGT(glm::vec3{ 164, 73, -48 }, glm::vec3{ 0, 0, 0 }, glm::vec3{ 10, 10, 10 });
	GeometricTransformation gt2 = getGT(glm::vec3{ 130, 43, 163 }, glm::vec3{ 0, -3.14, -3.14 / 30 }, glm::vec3{ 0.0001f, 0.0001f, 0.0001f });
	ufo->addLocalTransformKeyframe(gt1, 0.5f);
	ufo->addLocalTransformKeyframe(gt2, 3.0f);

	// VERY IMPORTANT
	m_viewer.setAnimationDuration(sceneNumber, 3.0f);
}

void Movie::ufoToSpace(int sceneNumber) {

	setSpaceSkybox(sceneNumber);

	glm::vec3 camPos{ 212.17, 64.41, -5.02 };
	glm::vec3 camForward{ -0.73, 0.04, 0.68 };
	glm::vec3 camUp{ -0.02, 0.99, -0.09 };

	m_viewer.setViewMatrix(sceneNumber, glm::lookAt(
		camPos,
		camPos + camForward,
		camUp));

	KeyframedMeshRenderablePtr ufo = std::make_shared<KeyframedMeshRenderable>(*m_ufo);
	ufo->setLocalTransform(GeometricTransformation(glm::vec3{ 164, 73, -48 }, glm::quat(glm::vec3{ 3.14 / 4, 3.14 / 2, 0 }), glm::vec3{ 10, 10, 10 }).toMatrix());

	m_viewer.addRenderable(sceneNumber, ufo);

	KeyframedMeshRenderablePtr earth = std::make_shared<KeyframedMeshRenderable>(*m_earth);
	earth->setLocalTransform(GeometricTransformation(glm::vec3{ 150, 0, 200 }, glm::quat(glm::vec3{ 0, 0, 0 }), glm::vec3{ 7, 7, 7 }).toMatrix());
	earth->addLocalTransformKeyframe(getGT(glm::vec3{ 150, 0, 200 }, glm::vec3{ 0, 0, 0 }, glm::vec3{ 7, 7, 7 }), 0.5f);
	earth->addLocalTransformKeyframe(getGT(glm::vec3{ 150, 0, 200 }, glm::vec3{ 0, -3.14 / 32, 0 }, glm::vec3{ 7, 7, 7 }), 3.0f);

	m_viewer.addRenderable(sceneNumber, earth);

	// ANIMATION
	GeometricTransformation gt1 = getGT(glm::vec3{ 164, 73, -48 }, glm::vec3{ 0, 0, 0 }, glm::vec3{ 10, 10, 10 });
	GeometricTransformation gt2 = getGT(glm::vec3{ 130, 43, 163 }, glm::vec3{ 0, -3.14, -3.14 / 30 }, glm::vec3{ 0.0001f, 0.0001f, 0.0001f });
	ufo->addLocalTransformKeyframe(gt2, 0.5f);
	ufo->addLocalTransformKeyframe(gt1, 3.0f);

	// VERY IMPORTANT
	m_viewer.setAnimationDuration(sceneNumber, 3.0f);
}

void Movie::docking(int sceneNumber) {

	setSpaceSkybox(sceneNumber);

	glm::mat4 localTransformation;
	glm::vec3 lightPosition(0, 1, 0);
	glm::vec3 d_direction = glm::normalize(glm::vec3(0, 1, 0));
	glm::vec3 d_ambient(0.3, 0.3, 0.3), d_diffuse(0.75, 0.75, 0.75), d_specular(0.6, 0.6, 0.6);
	DirectionalLightPtr directionalLight = std::make_shared<DirectionalLight>(d_direction, d_ambient, d_diffuse, d_specular);
	m_viewer.setDirectionalLight(sceneNumber, directionalLight);

	glm::vec3 camPos{ -15.0, 5.79, -2.11 };
	glm::vec3 camForward{ 0.88, -0.36, 0.31 };
	glm::vec3 camUp{ 0.35, 0.93, 0.096 };

	m_viewer.setViewMatrix(sceneNumber, glm::lookAt(
		camPos,
		camPos + camForward,
		camUp));

	float p_constant = 1.0, p_linear = 5e-1, p_quadratic = 0.0;

	for (int i = 0; i < 10; i += 1) {
		m_viewer.addPointLight(sceneNumber, std::make_shared<PointLight>(glm::vec3(i*20, 3, 0), d_ambient, d_diffuse, d_specular, p_constant, p_linear, p_quadratic));
		//viewer.addPointLight(std::make_shared<PointLight>(glm::vec3(i, 3, -20), d_ambient, d_diffuse, d_specular, p_constant, p_linear, p_quadratic));
		//viewer.addPointLight(std::make_shared<PointLight>(glm::vec3(i, 3, 20), d_ambient, d_diffuse, d_specular, p_constant, p_linear, p_quadratic));
	}

	//Object instanciations, adding them to viewer and creating a hierarchy
	KeyframedMeshRenderablePtr dock = std::make_shared<KeyframedMeshRenderable>(*m_corridor);
	dock->setLocalTransform(GeometricTransformation(glm::vec3{ 0,0,0 }, glm::quat(glm::vec3{ 0,M_PI,0 }), glm::vec3{ 2,2,2 }).toMatrix());

	KeyframedMeshRenderablePtr ufo = std::make_shared<KeyframedMeshRenderable>(*m_ufo);
	ufo->setLocalTransform(GeometricTransformation(glm::vec3{ 0,0,0 }, glm::quat(glm::vec3{ 0,0,0 }), glm::vec3{ 4,4,4 }).toMatrix());

	m_viewer.addRenderable(sceneNumber, dock);
	m_viewer.addRenderable(sceneNumber, ufo);
	KeyframedMeshRenderable::addChild(dock, ufo);

	//========= Animation =========//

	GeometricTransformation gt1 = getGT(glm::vec3{ 0, 1.0f,0 }, glm::vec3{ 0,0,0 }, glm::vec3{ 1,1,1 });
	GeometricTransformation gt2 = getGT(glm::vec3{ 5.0f, 1.0f,0.0f }, glm::vec3{ 0,3.1415/8,0 }, glm::vec3{ 1,1,1 });
	GeometricTransformation gt3 = getGT(glm::vec3{ 10.0f,1.0f,0.0f }, glm::vec3{ 0,3.1415 / 4,0 }, glm::vec3{ 1,1,1 });
	GeometricTransformation gt4 = getGT(glm::vec3{ 15.0f,-1.0f,0.0f }, glm::vec3{ 0,3*3.1415 / 8,-3.1415 / 20 }, glm::vec3{ 1,1,1 });
	GeometricTransformation gt5 = getGT(glm::vec3{ 20.0f,-3.0f,-1.5f }, glm::vec3{ 0,3.1415 / 2,-3.1415 / 15 }, glm::vec3{ 1,1,1 });
	GeometricTransformation gt6 = getGT(glm::vec3{ 25.0f,-7.0f,-3.0f }, glm::vec3{ 0,5*3.1415 / 8,-3.1415 / 10 }, glm::vec3{ 1,1,1 });
	ufo->addLocalTransformKeyframe(gt1, 1.0f);
	ufo->addLocalTransformKeyframe(gt2, 1.5f);
	ufo->addLocalTransformKeyframe(gt3, 2.0f);
	ufo->addLocalTransformKeyframe(gt4, 2.5f);
	ufo->addLocalTransformKeyframe(gt5, 3.0f);
	ufo->addLocalTransformKeyframe(gt6, 3.5f);

	// VERY IMPORTANT
	m_viewer.setAnimationDuration(sceneNumber, 3.5f);
}

// ---------------------------------------------------------------------------------------------------------------------------------------
// Skyboxes
// ---------------------------------------------------------------------------------------------------------------------------------------
void Movie::loadEarthSkybox()
{
    std::string path = "../../sfmlGraphicsPipeline/textures/skybox/hill/";
    std::vector<std::string> faces =
        {
			path + "px.jpg",
			path + "nx.jpg",
			path + "py.jpg",
			path + "ny.jpg",
			path + "pz.jpg",
			path + "nz.jpg" };

    m_earthSkybox = std::make_shared<SkyboxRenderable>(m_skyboxShader, faces);
}

void Movie::setEarthSkybox(int sceneIndex)
{
    m_viewer.setSkybox(sceneIndex, m_earthSkybox);

    // Defining the light of the space skybox
    glm::vec3 lightPosition(1.6, 233.7, 175.6);
    glm::vec3 d_direction = glm::normalize(glm::vec3(0, 30, 0) - lightPosition);
    glm::vec3 d_ambient(0.1, 0.1, 0.1), d_diffuse(0.8, 0.8, 0.8), d_specular(0.8, 0.8, 0.8);
    DirectionalLightPtr directionalLight = std::make_shared<DirectionalLight>(d_direction, d_ambient, d_diffuse, d_specular);

    m_viewer.setDirectionalLight(sceneIndex, directionalLight);
}

void Movie::loadSpaceSkybox()
{
    std::string path = "../../sfmlGraphicsPipeline/textures/skybox/space_skybox/";
    std::vector<std::string> faces =
        {
		   path + "right.png",
			path + "left.png",
			path + "top.png",
			path + "bottom.png",
			path + "front.png",
			path + "back.png" };

    m_spaceSkybox = std::make_shared<SkyboxRenderable>(m_skyboxShader, faces);
}

void Movie::setSpaceSkybox(int sceneIndex)
{
    m_viewer.setSkybox(sceneIndex, m_spaceSkybox);

    // Defining the light of the space skybox
    glm::vec3 lightPosition(-35.0, 17.0, 0.0);
    glm::vec3 d_direction = glm::normalize(-lightPosition);
    glm::vec3 d_ambient(0.1, 0.1, 0.1), d_diffuse(0.8, 0.8, 0.8), d_specular(0.8, 0.8, 0.8);
    DirectionalLightPtr directionalLight = std::make_shared<DirectionalLight>(d_direction, d_ambient, d_diffuse, d_specular);

    m_viewer.setDirectionalLight(sceneIndex, directionalLight);
}
