#include "../include/Texture.hpp"
#include <iostream>
#include "../include/Image.hpp"

Texture::Texture() : Texture("./../../sfmlGraphicsPipeline/textures/missing_texture.png") {}

Texture::Texture(std::string filename) : m_texFilename(filename) {
    
    //Create texture
    glcheck(glGenTextures(1, &m_texId));

    //Bind the texture
    glcheck(glBindTexture(GL_TEXTURE_2D, m_texId));

    //Textured options
    glcheck(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST));
    glcheck(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST));
    glcheck(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE));
    glcheck(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE));

    Image img;
    bool loaded = img.load(m_texFilename);

    if(!loaded)
    {
        std::cout << "Failed to load texture at " << filename << std::endl;
        m_texFilename = MISSING_TEXTURE_PATH;
        loaded = img.load(m_texFilename);
    }
        

    if(loaded)
    {
        int nrChannels = img.nrChannels();
        // Looking for the format
        assert((1 <= nrChannels) && (4 >= nrChannels));
        GLenum glformat;
        switch (nrChannels)
        {
        case 1:
            glformat = GL_RED;
            break;
        case 2:
            glformat = GL_RG;
            break;
        case 3:
            glformat = GL_RGB;
            break;
        case 4:
            glformat = GL_RGBA;
            break;
        }
        
        glcheck(glTexImage2D(GL_TEXTURE_2D, 0, glformat, img.width(), img.height(), 0, glformat, GL_UNSIGNED_BYTE, img.data()));
        glcheck(glGenerateMipmap(GL_TEXTURE_2D));
    }
    else
    {
        std::cout << "Fatal error : no path to missing texture. The program is going to crash. " << std::endl;
    }
    

    //Release the texture
    glcheck(glBindTexture(GL_TEXTURE_2D, 0));
}

void Texture::render(const ShaderProgramPtr& shaderPrg, std::string nameInShader, int nbTexture)
{
    //std::cout << " * * * * * * * * * * image: " << m_texFilename << " nameInShader: " << nameInShader << " texId: " << m_texId << " nbTex: " << nbTexture << " * * * * * * * * * " << std::endl;
    int texSampleLoc = shaderPrg->getUniformLocation(nameInShader);
    glcheck(glActiveTexture(GL_TEXTURE0 + nbTexture));
    glcheck(glBindTexture(GL_TEXTURE_2D, m_texId));
    //Send "texSampler" to Textured Unit "nbTexture"
    glcheck(glUniform1i(texSampleLoc, nbTexture));
}

void Texture::bind()
{
    //Bind the texture
    glcheck(glBindTexture(GL_TEXTURE_2D, m_texId));
}

void Texture::release(int nbTexture)
{
    //Release texture
    glcheck(glActiveTexture(GL_TEXTURE0 + nbTexture));
    glcheck(glBindTexture(GL_TEXTURE_2D, 0));
}

void Texture::setWrapOption(int wrapOption)
{
    //Textured options
    if (wrapOption == 0)
    {
        glcheck(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE));
        glcheck(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE));
    }
    else if (wrapOption == 1)
    {
        glcheck(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT));
        glcheck(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT));
    }
    else if (wrapOption == 2)
    {
        glcheck(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT));
        glcheck(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT));
    }
}

void Texture::setFilterOption(int filterOption)
{
    if (filterOption == 0)
    {
        glcheck(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST));
        glcheck(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST));
    }
    else if (filterOption == 1)
    {
        glcheck(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR));
        glcheck(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR));
    }
    else if (filterOption == 2)
    {
        glcheck(glGenerateMipmap(GL_TEXTURE_2D));
        glcheck(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR));
        glcheck(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR));
    }
}

Texture::~Texture()
{
    glcheck(glDeleteTextures(1, &m_texId));
}