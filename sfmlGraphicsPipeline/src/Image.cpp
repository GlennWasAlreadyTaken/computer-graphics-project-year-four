#include "../include/Image.hpp"

bool Image::load(std::string filename, bool flipVertically)
{
    stbi_set_flip_vertically_on_load(flipVertically);
    
    m_data = stbi_load(filename.c_str(), &m_width, &m_height, &m_nrChannels, 0);

    return m_data;
}

unsigned char * Image::data()
{
    return m_data;
}

int Image::width()
{
    return m_width;
}

int Image::height()
{
    return m_height;
}

int Image::nrChannels()
{
    return m_nrChannels;
}

Image::~Image() 
{
    stbi_image_free(m_data);
}
