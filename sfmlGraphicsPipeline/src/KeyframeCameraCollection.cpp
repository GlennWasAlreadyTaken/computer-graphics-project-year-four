# include "./../include/KeyframeCameraCollection.hpp"


void KeyframeCameraCollection::add( const CameraTransformation& transformation, float time )
{
    m_keyframes.insert( std::make_pair(time, transformation) );
}

glm::mat4 KeyframeCameraCollection::interpolateTransformation( float time ) const
{
    //TODO: Complete the interpolation framework
    if( !m_keyframes.empty() )
    {
        //Handle the case where the time parameter is outside the keyframes time scope.
        std::map< float, CameraTransformation >::const_iterator itFirstFrame = m_keyframes.begin();
        std::map< float, CameraTransformation >::const_reverse_iterator itLastFrame = m_keyframes.rbegin();
        if( time <= itFirstFrame->first ) return itFirstFrame->second.toMatrix();
        if( time >= itLastFrame->first ) return itLastFrame->second.toMatrix();

        //Get keyframes surrounding the time parameter
        std::array< Keyframe, 2 > result = getBoundingKeyframes( time );

        //TODO: Compute the interpolating factor based on the time parameter and the surrounding keyframes times.
        float factor = (time - result[0].first) / (result[1].first - result[0].first);

        //TODO: Interpolate each transformation component of the surrounding keyframes: orientation, translation, scale
        //      Use spherical linear interpolation for the orientation interpolation, glm::slerp(value1, value2, factor);
        //      Use linear interpolation for the translation and scale, glm::lerp(value1, value2, factor);
        CameraTransformation g = CameraTransformation::interpolate(result[0].second, result[1].second, factor);

        // Create quaternions from the rotation matrices produced by glm::lookAt
        /*
        CameraTransformation g1 = result[0].second;
        CameraTransformation g2 = result[1].second;

        glm::quat quat_start(glm::lookAt(g1.getPosition(), g1.getPosition() + g1.getForward(), g1.getUp()));
        glm::quat quat_end(glm::lookAt(g2.getPosition(), g2.getPosition() + g2.getForward(), g2.getUp()));

        // First interpolate the rotation
        glm::quat quat_interp = glm::slerp(quat_start, quat_end, factor);

        // Then interpolate the translation
        glm::vec3 pos_interp = glm::mix(g1.getPosition(), g2.getPosition(), factor);

        glm::mat4 view_matrix = glm::mat4_cast(quat_interp); // Setup rotation
        view_matrix[3] = glm::vec4(pos_interp, 1.0);         // Introduce translation

        //Build a matrix transformation
        return view_matrix;
        */

       return g.toMatrix();
    }
    else
    {
        return glm::mat4(1.0);
    }
}

bool KeyframeCameraCollection::empty() const
{
    return m_keyframes.empty();
}

std::array< KeyframeCameraCollection::Keyframe, 2 > KeyframeCameraCollection::getBoundingKeyframes( float time ) const
{
    std::array< KeyframeCameraCollection::Keyframe, 2 > result{ std::make_pair(0, CameraTransformation()), std::make_pair(0, CameraTransformation()) };
    std::map< float, CameraTransformation >::const_iterator upper = m_keyframes.upper_bound(time);
    std::map< float, CameraTransformation >::const_iterator lower = std::prev(upper);
    std::map< float, CameraTransformation >::const_iterator end = m_keyframes.end();
    if(upper != end && lower != end )
    {
        result[0] = *lower;
        result[1] = *upper;
    }
    return result;
}
